# Description

This is the repository for the TTS Mod MMGA which is a TTS version of the
boardgame Terraforming Mars.

# Organization

lua/ has the main code for the objects and Global
luaIncludes/ has all the included code that is reused across multiple files/splits Global
save/ holds the reference savegames

# License

All lua code in this repository written by me is under MIT license, as I like
for others to be able to use/share my code as they see fit, and have absolutely no
intention of profiting from this project in any financial form.

The exception to this are:

- randomizer.ttslua which is left over from the original
- Discard.*.ttslua which are left over from the original
- TM PLaymat.*.ttslua which are left over from the original
- Custom Tile.*.ttslua which are left over from the original
- Custom Token.71af47.ttslua which are left over from the original
- Custom Token.c1af2b.ttslua which are left over from the original

- parts of ScoreCounter.*.ttslua which were made by MrStump
- parts of genericCounter.*.ttslua which were made by MrStump
- parts of genericProductionCounter.*.ttslua which were made by MrStump

# Savegame License

The savegame JSON file's license in the save/ folder is a bit of an unknown.
Technically its made by multiple people over a long period of time with no known
license attached to it.

The content of the savegame which links to boardgame assets makes it clear that
it isn't for public distribution. The usage should be purely for closed groups,
where it is clearly expected, that the people in the group own the boardgames,
or at least those people hosting the game do.

I'd like to state clearly here that it is not the intention of this project to
infringe upon Fryxgames property but merely to provide a way for boardgamers to
play the games they already own in some fashion to play online with all the
freedom that the boardgame offers in regards to setup/expansions etc..

Support Fryxgames continuing effort to create/improve
a great game by buying their games!

# Contributing

Contributions to the code in the form of merge requests are welcome. The license
for code contributions should be clear and one of the open source licenses that
is not copy-left (i.e. GPL) (its unclear to me if GPL code and TTS would
theoretically work... probably not though)

If you want to make sure that a feature will be welcome before making the effort
of implementing it, open an issue first or check in the discord suggestions
channel. Probably most things that are in the spirit of the community will be merged.

If you are contributing savegame changes. Make the changes either directly in the
savegame, or if you need to use TTS then disable scripting while making the changes,
overwrite the savegame in the git repository, and then ONLY commit the neccessary changes.
TTS likes to change a lot of things on load/save that are not neccessary to change.

See also:

- [Building.md](./Building.md)
- [HowToMakeScriptChanges.md](./HowToMakeScriptChanges.md)
- [HowToMakeTableChanges.md](./HowToMakeTableChanges.md)

Frequently a large feature is consistent of multiple small changes.
If you commit regularly then it is much easier to follow your thought processes.

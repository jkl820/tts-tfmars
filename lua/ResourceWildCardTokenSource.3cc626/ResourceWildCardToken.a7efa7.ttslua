#include buildingBlocks/baseStatefulObject

defaultObjectState.tokenIsBeingAbsorbed = false
defaultObjectState.tokenType = "ResourceToken"
defaultObjectState.amount = 1
defaultObjectState.validResources = { "Credits", "Steel", "Titanium", "Plants", "Energy", "Heat" }
self.setName("ResourceToken: Any Resource")

function initializeWithMetdata(params)
  local metadata = params.metadata

  if metadata == nil then
    -- don't fail, just use defaults
    metadata = {}
  end

  objectState.tokenType = metadata.tokenType or "ResourceToken"
  objectState.amount = metadata.amount or 1
  local validResources = metadata.validResources or { "Credits", "Steel", "Titanium", "Plants", "Energy", "Heat" }

  local tokenUserFriendlyName = objectState.tokenType..": "
  for index, resource in pairs(validResources) do
    tokenUserFriendlyName = tokenUserFriendlyName.." "..resource
  end

  if objectState.tokenType == "ProductionToken" then
    for index, resource in pairs(validResources) do
      validResources[index] = resource.." Production"
    end
  end

  self.setName(tokenUserFriendlyName)
  objectState.validResources = validResources
end

function onCollisionEnter(collision_info)
  local collisionPartner = collision_info.collision_object
  local acceptedTokens = collisionPartner.getVar("acceptedTokens")
  if acceptedTokens ~= nil then
    for _, validResource in pairs(objectState.validResources) do
      if validResource == collisionPartner.getName() then
        for _, entry in pairs(acceptedTokens) do
          if entry == objectState.tokenType and not objectState.tokenIsBeingAbsorbed then
            objectState.tokenIsBeingAbsorbed = true
            if objectState.amount >= 0 then
              for i=1, objectState.amount do
                collisionPartner.call("increase")
              end
              self.destruct()
            else
              for i=1, -objectState.amount do
                collisionPartner.call("decrease")
              end
              self.destruct()
            end
          end
        end
      end
    end
  elseif type(collisionPartner.getVar("getAcceptedTokenList")) == "function" then
    if not collisionPartner.getVar("cardState").active then
      return
    end

    local acceptedTokensList = collisionPartner.call("getAcceptedTokenList")
    for _,acceptedToken in ipairs(acceptedTokensList) do
      for _, validResource in pairs(objectState.validResources) do
        if acceptedToken == validResource and not objectState.tokenIsBeingAbsorbed then
          objectState.tokenIsBeingAbsorbed = true
          collisionPartner.call("addTokensToCard", { quantity = 1, tokenName = validResource })
          self.destruct()
        end
      end
    end
  end
end

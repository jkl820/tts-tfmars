# Building the mod after changes

Get the TTSLuaExtrator from here:

https://gitlab.com/mmga/TTSLuaExtractor

Build it with dotnet or Visual Studio (or download from the releaes page) and add it to your path.

Then build a patched save game by running this command line in the git repository folder:

TTSLuaExtractor embed .\save\TFMars_ReferenceSave.json .\release\TFMARS_vVERSION.json .\lua\ .\luaIncludes\

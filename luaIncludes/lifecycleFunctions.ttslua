function onLoad(saved_data)
  local loaded_data

	-- Reset save data on version upgrade
  if saved_data ~= "" and saved_data ~= nil then
    loaded_data = JSON.decode(saved_data)
    if loaded_data.scriptVersion ~= scriptVersion then
      saved_data = nil
    end
  end

  if saved_data ~= "" and saved_data ~= nil then
    print("Loading data!")
    loaded_data = JSON.decode(saved_data)
    gameState = loaded_data.gameState
    gameMap = hexMapHelpers.makeMapComputeFriendly(loaded_data.map)
    if loaded_data.venusMap ~= nil then
      venusMap = hexMapHelpers.makeMapComputeFriendly(loaded_data.venusMap)
    end
    gameEventHandling = loaded_data.gameEventHandling
    globalParameterSystem.values = loaded_data.globalParameters
    logging.currentMode = loaded_data.loggingMode
    gameConfig = loaded_data.gameConfig
    bagProtectorAllowedList = loaded_data.bagProtectorAllowedList
    bagProtectorGuidStores.generalStore = loaded_data.bagProtectorGeneralStore
  end
  tablePositions.update()
  board.update()
  -- Wait a bit after load to spawn in everything
  Wait.time(function()
    createOrUpdatePlayerColors()
    updatePlayerUI()
  end, 1)
  math.randomseed( os.time() )

  if gameState.started ~= true then
  	createAllSetupButtons()
    milestoneSystem.initialize()
    awardSystem.initialize()
  else
    setupMapButtons()
    setupStandardProjectMat()
    globalParameterSystem.setupButtons()
    setupBoard()
  end

  if gameState.activeExpansions.timer then
    timerFunctions.initializeTimer()
  end

  readGameConfigOnGameLoad()
  cardBanning.initialize()
  makeObjectsUninteractable()
end

function onSave()
  local serializableMap = hexMapHelpers.makeMapSerializable(gameMap)
  local data_to_save = nil
  if venusMap ~= nil then
    local serializableVenusMap = hexMapHelpers.makeMapSerializable(venusMap)
    data_to_save = {scriptVersion = scriptVersion, gameEventHandling = gameEventHandling, gameState = gameState, bagProtectorAllowedList = bagProtectorAllowedList, bagProtectorGeneralStore = bagProtectorGuidStores.generalStore, map = serializableMap, loggingMode = logging.currentMode, globalParameters = globalParameterSystem.values, venusMap = serializableVenusMap, gameConfig = gameConfig}
  else
    data_to_save = {scriptVersion = scriptVersion, gameEventHandling = gameEventHandling, gameState = gameState, bagProtectorAllowedList = bagProtectorAllowedList, bagProtectorGeneralStore = bagProtectorGuidStores.generalStore, map = serializableMap, loggingMode = logging.currentMode, globalParameters = globalParameterSystem.values, gameConfig = gameConfig}
  end

  return JSON.encode(data_to_save)
end

-- test function for debugging the infamous discard issue
-- runLoop = false
-- function discardLoop()
--   function discardLoopCoroutine()
--     while runLoop == true do
--       for i=1,100 do
--         coroutine.yield(0)
--       end
--       local cardsInHand = Player["White"].getHandObjects()
--
--       if #cardsInHand > 0 then
--         Global.call("objectActivationSystem_doAction", { activationEffects = { effects = {"DiscardCard","DiscardCard", "DiscardCard"} },
--           sourceName = " discard loop.",
--           playerColor = "White",
--           object = nil })
--       end
--     end
--     return 1
--   end
--   startLuaCoroutine(self, "discardLoopCoroutine")
-- end

function filterObjectEnterContainer(container, enter_object)
    local containerGuid = container.getGUID()
    local enterObjectGuid = enter_object.getGUID()
    local isOcean = gameObjectHelpers.isOcean(enterObjectGuid)
    local bagProtectorResult =  bagProtector.filterObjectEnter(container, enter_object)

    if bagProtectorResult ~= nil then
      return bagProtectorResult
    end

    local isCity = gameObjectHelpers.isCity(enterObjectGuid)

    if containerGuid == gameObjectHelpers.getGuidByName("specialsBag") and (enterObjectGuid == gameObjectHelpers.getGuidByName("capitalCityToken") or enterObjectGuid == gameObjectHelpers.getGuidByName("oceanCityToken") or enterObjectGuid == gameObjectHelpers.getGuidByName("redCity")) then
      return true
    end

    if containerGuid ~= gameObjectHelpers.getGuidByName("genericCityBag") and isCity then
      logging.printToAll("Cities only belong on the board or in the city bag", {1,1,1}, loggingModes.essential)
      return false
    end

    local isGreenery = gameObjectHelpers.isGreenery(enter_object)

    if containerGuid ~= gameObjectHelpers.getGuidByName("genericGreeneryBag") and isGreenery then
      logging.printToAll("Greenerys only belong on the board or in the city bag", {1,1,1}, loggingModes.essential)
      return false
    end

    log("Enter container allowed!")

    return true
end

-- Helper tables to discern whether an object is actually dead *sigh*
objectsOfInterest = {}
objectsEnteredContainer = {}

function onObjectEnterContainer(container, enter_object)
  local enterGuid = enter_object.getGUID()
  if objectsOfInterest[enterGuid] ~= nil then
    objectsOfInterest[enterGuid] = nil
    onContainerDeath(container, enter_object)
  else
    objectsEnteredContainer[enterGuid] = container
  end
end


-- Is called on delete AND enter bag
function onObjectDestroy(dying_object)
  if dying_object == nil then
    return
  end

	local dyingGuid = dying_object.getGUID()

  if objectsEnteredContainer[dyingGuid] ~= nil then
      objectsEnteredContainer[dyingGuid] = nil
      onContainerDeath(container, dying_object)
  -- The else part is for objects that really died but something else might have happened ....
  else
    local objectOfInterestStructure = {
      timeOfDeath = Time.time
    }

    Wait.frames(function()
      if objectsOfInterest ~= nil then
        objectsOfInterest[dyingGuid] = objectOfInterestStructure

        Wait.time(checkObjectRealDeath, 0.1)
      end
    end, 1)
  end
end

function checkObjectRealDeath()
    for guid,objectStructure in pairs(objectsOfInterest) do
      if (Time.time - objectStructure.timeOfDeath) > 0.05 then
          objectsOfInterest[guid] = nil
          onRealDeath(guid)
      end
    end
end

function onContainerDeath(container, dying_object)
  local dyingGuid = dying_object.getGUID()
	if gameObjectHelpers.isOcean(dyingGuid) then
    gameObjectHelpers.oceanStashed()
	elseif gameObjectHelpers.isCity(dyingGuid) then
    gameObjectHelpers.cityDestroyed(dyingGuid)
  elseif gameObjectHelpers.isGreenery(dyingGuid) then
    gameObjectHelpers.greeneryDestroyed(dyingGuid)
  end
end

function onRealDeath(dyingGuid)
	-- Do not allow destruction of oceans
	if gameObjectHelpers.isOcean(dyingGuid) then
    gameObjectHelpers.oceanDestroyed(dyingGuid)
    logging.printToAll("Making sure no oceans are lost!", {1,1,1}, loggingModes.unimportant)
    ensureBagOceans()
	end
end

function onObjectLeaveContainer(container, leave_object)
	  function handleObjectDelayed()
      -- Wait ten frames so GUID conflicts can be handled
      for i=1,10 do
        coroutine.yield(0)
      end
      local containerId = container.getGUID()
    	local objectId = leave_object.getGUID()

      if leave_object == nil then
        return 1
    	elseif containerId == gameObjectHelpers.getGuidByName("specialsBag") then
        updateCityCounters()
    	elseif gameObjectHelpers.isOcean(objectId) then
        gameObjectHelpers.oceanRetrieved()
      elseif leave_object.name == 'Card' then
        handleCardLeavingContainer(containerId, leave_object)
      end

      bagProtector.objectLeaveContainer(container, leave_object)

      return 1
    end
    startLuaCoroutine(self, "handleObjectDelayed")
end

function handleCardLeavingContainer(containerId, leave_object)
  if gameObjectHelpers.getGuidByName("turmoilDeck") == containerId then -- do not touch turmoil cards
    return
  end

  if gameObjectHelpers.getGuidByName("corpDeck") == containerId then
    leave_object.setDescription(":Corporation:\n"..leave_object.getDescription())
  elseif gameObjectHelpers.getGuidByName("preludeDeck") == containerId then
    leave_object.setDescription(":Prelude:\n"..leave_object.getDescription())
  end

  if leave_object.getLuaScript() == nil or leave_object.getLuaScript() == '' then
    local baseCardScript = getCardSpecificLuaScript(leave_object)
    leave_object.setLuaScript(baseCardScript)
  end
end

function getCardSpecificLuaScript(card)
  local cardName = card.getName()
  if string.match(cardName, "%(B%)") then
    return getObjectFromGUID(gameObjectHelpers.getGuidByName("baseBlueCard")).getLuaScript()
  elseif string.match(cardName, "%(E%)") then
    return getObjectFromGUID(gameObjectHelpers.getGuidByName("baseEventCard")).getLuaScript()
  elseif string.match(cardName, "%(G%)") then
    return getObjectFromGUID(gameObjectHelpers.getGuidByName("baseGreenCard")).getLuaScript()
  elseif string.match(cardName, "%(HO%)") then
    return getObjectFromGUID(gameObjectHelpers.getGuidByName("baseInfrastructureCard")).getLuaScript()
  elseif string.match(cardName, "%(C%)") then
    return getObjectFromGUID(gameObjectHelpers.getGuidByName("baseCorporationCard")).getLuaScript()
  else
    return getObjectFromGUID(gameObjectHelpers.getGuidByName("baseCard")).getLuaScript()
  end
end

function onPlayerChangeColor(player_color)
  Wait.frames(function()
    if gameState == nil then
      return
    end

  	if gameState.started ~= true then
  		createOrUpdatePlayerColors()
  	end
  	updatePlayerUI()
  end, 1)
end

predefinedMapSettings = {}
predefinedMapSettings.default = {
  prettyName = "Default",
  description = "The default settings.",
  config = {
    mapGeneratorConfig = {
      mapSizeSelection = 3,
      deltaFactor = 0.1,
      bonusTilesRatio = 8/10,
      totallyRandomTilesRatio = 0.1,
      tileEffects = {
        Plants = { weighting = 10, averageTileYield = 1.8, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Titanium = { weighting = 2, averageTileYield = 1, yieldDiffusion = 0.4, maxYield = 2, seedPoints = 5, shapeFactor = 0.6, seedDistance = 4 },
        Steel = { weighting = 5, averageTileYield = 1.7, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 5, shapeFactor = 0.8, seedDistance = 4 },
        DrawCard = { weighting = 3, averageTileYield = 1.45, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 200, shapeFactor = 0.2, seedDistance = 4 },
        Heat = { weighting = 0, averageTileYield = 2, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Energy = { weighting = 0, averageTileYield = 1.5, yieldDiffusion = 0.25, maxYield = 4, seedPoints = 2, shapeFactor = 2, seedDistance = 4 },
        Credits = { weighting = 0, averageTileYield = 5, yieldDiffusion = 0.5, maxYield = 6, seedPoints = 5, shapeFactor = 0.2, seedDistance = 1 },
        OtherEffects = { weighting = 0, averageTileYield = 0.0, yieldDiffusion = 0.0, maxYield = 0, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 } },
      oceanSettings = {
        oceanSeedPoints = 4,
        oceanSeedMinDistance = 4,
        oceanShapeFactor = 1, -- values >= 1 --> bulky cluster, values < 1 --> snakey ocean lines
      },
      absoluteTiles = {
        volcanoTiles = 3,
        blockedTiles = 0, -- blocked tiles give adjancency bonuses (e.g. draw 1 card, gain 1 plant, etc)
        initialErosions = 0,
        initialDuststorms = 3,
      }
    }
  }
}

predefinedMapSettings.firstExplorers = {
  prettyName = "First Explorers",
  description = "Lots of card draw bonuses. Besides that only few other bonsuses.",
  config = {
    mapGeneratorConfig = {
      mapSizeSelection = 3,
      deltaFactor = 0.1,
      bonusTilesRatio = 16/10,
      totallyRandomTilesRatio = 0.05,
      tileEffects = {
        Plants = { weighting = 3, averageTileYield = 1.5, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Titanium = { weighting = 1, averageTileYield = 1, yieldDiffusion = 0.25, maxYield = 2, seedPoints = 5, shapeFactor = 0.6, seedDistance = 4 },
        Steel = { weighting = 2, averageTileYield = 1, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 5, shapeFactor = 0.8, seedDistance = 4 },
        DrawCard = { weighting = 11, averageTileYield = 1.8, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 5, shapeFactor = 0.1, seedDistance = 4 },
        Heat = { weighting = 1, averageTileYield = 1.8, yieldDiffusion = 0.25, maxYield = 4, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Energy = { weighting = 0, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 2, shapeFactor = 2, seedDistance = 4 },
        Credits = { weighting = 0, averageTileYield = 5, yieldDiffusion = 0.5, maxYield = 6, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 },
        OtherEffects = { weighting = 2, averageTileYield = 0.0, yieldDiffusion = 0.0, maxYield = 0, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 } },
      oceanSettings = {
        oceanSeedPoints = 3,
        oceanSeedMinDistance = 4,
        oceanShapeFactor = 0.5, -- values >= 1 --> bulky cluster, values < 1 --> snakey ocean lines
      },
      absoluteTiles = {
        volcanoTiles = 4,
        blockedTiles = 0, -- blocked tiles give adjancency bonuses (e.g. draw 1 card, gain 1 plant, etc)
        initialErosions = 0,
        initialDuststorms = 3,
      }
    }
  }
}

predefinedMapSettings.martianDesert = {
  prettyName = "Martian Desert",
  description = "Few bonus spaces at all. Also additional erosions and duststorms"..
    "if you play with the Ares fan expansion.",
  config = {
    mapGeneratorConfig = {
      mapSizeSelection = 3,
      deltaFactor = 0.1,
      bonusTilesRatio = 1/10,
      totallyRandomTilesRatio = 0.01,
      tileEffects = {
        Plants = { weighting = 1, averageTileYield = 2, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Titanium = { weighting = 3, averageTileYield = 1, yieldDiffusion = 0.5, maxYield = 2, seedPoints = 5, shapeFactor = 0.6, seedDistance = 4 },
        Steel = { weighting = 5, averageTileYield = 1.7, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 5, shapeFactor = 0.8, seedDistance = 4 },
        DrawCard = { weighting = 1, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 5, shapeFactor = 0.2, seedDistance = 4 },
        Heat = { weighting = 0, averageTileYield = 2, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Energy = { weighting = 0, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 2, shapeFactor = 2, seedDistance = 4 },
        Credits = { weighting = 0, averageTileYield = 5, yieldDiffusion = 0.5, maxYield = 6, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 },
        OtherEffects = { weighting = 0, averageTileYield = 0.0, yieldDiffusion = 0.0, maxYield = 0, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 } },
      oceanSettings = {
        oceanSeedPoints = 2,
        oceanSeedMinDistance = 4,
        oceanShapeFactor = 1, -- values >= 1 --> bulky cluster, values < 1 --> snakey ocean lines
      },
      absoluteTiles = {
        volcanoTiles = 4,
        blockedTiles = 0, -- blocked tiles give adjancency bonuses (e.g. draw 1 card, gain 1 plant, etc)
        initialErosions = 4,
        initialDuststorms = 5,
      }
    }
  }
}

predefinedMapSettings.richDeposits = {
  prettyName = "Rich Deposits",
  description = "Few plants and card draw locations. Lots of high steel and titanium yield tiles. One single ocean area.",
  config =
  {
    mapGeneratorConfig = {
      mapSizeSelection = 3,
      deltaFactor = 0.1,
      bonusTilesRatio = 14/10,
      totallyRandomTilesRatio = 0.01,
      tileEffects = {
        Plants = { weighting = 1, averageTileYield = 2, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Titanium = { weighting = 10, averageTileYield = 1.7, yieldDiffusion = 0.5, maxYield = 2, seedPoints = 5, shapeFactor = 0.6, seedDistance = 4 },
        Steel = { weighting = 15, averageTileYield = 2.7, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 5, shapeFactor = 0.8, seedDistance = 4 },
        DrawCard = { weighting = 1, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 5, shapeFactor = 0.2, seedDistance = 4 },
        Heat = { weighting = 0, averageTileYield = 2, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Energy = { weighting = 0, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 2, shapeFactor = 2, seedDistance = 4 },
        Credits = { weighting = 0, averageTileYield = 5, yieldDiffusion = 0.5, maxYield = 6, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 },
        OtherEffects = { weighting = 0, averageTileYield = 0.0, yieldDiffusion = 0.0, maxYield = 0, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 } },
      oceanSettings = {
        oceanSeedPoints = 4,
        oceanSeedMinDistance = 4,
        oceanShapeFactor = 0.5, -- values >= 1 --> bulky cluster, values < 1 --> snakey ocean lines
      },
      absoluteTiles = {
        volcanoTiles = 3,
        blockedTiles = 0, -- blocked tiles give adjancency bonuses (e.g. draw 1 card, gain 1 plant, etc)
        initialErosions = 0,
        initialDuststorms = 3,
      }
    }
  }
}

predefinedMapSettings.fastTerraformers = {
  prettyName = "Fast Terraformers",
  description = "Lots of plants and heat bonuses. Very few card draw locations. No titanium, a bit of steel.",
  config =
  {
    mapGeneratorConfig = {
      mapSizeSelection = 3,
      deltaFactor = 0.1,
      bonusTilesRatio = 4,
      totallyRandomTilesRatio = 0.01,
      tileEffects = {
        Plants = { weighting = 13, averageTileYield = 2.5, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Titanium = { weighting = 0, averageTileYield = 1, yieldDiffusion = 0.5, maxYield = 2, seedPoints = 5, shapeFactor = 0.6, seedDistance = 4 },
        Steel = { weighting = 2, averageTileYield = 1.7, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 5, shapeFactor = 0.8, seedDistance = 4 },
        DrawCard = { weighting = 1, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 5, shapeFactor = 0.2, seedDistance = 4 },
        Heat = { weighting = 13, averageTileYield = 3, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Energy = { weighting = 0, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 2, shapeFactor = 2, seedDistance = 4 },
        Credits = { weighting = 0, averageTileYield = 5, yieldDiffusion = 0.5, maxYield = 6, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 },
        OtherEffects = { weighting = 0, averageTileYield = 0.0, yieldDiffusion = 0.0, maxYield = 0, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 } },
      oceanSettings = {
        oceanSeedPoints = 3,
        oceanSeedMinDistance = 4,
        oceanShapeFactor = 0, -- values >= 1 --> bulky cluster, values < 1 --> snakey ocean lines
      },
      absoluteTiles = {
        volcanoTiles = 3,
        blockedTiles = 0, -- blocked tiles give adjancency bonuses (e.g. draw 1 card, gain 1 plant, etc)
        initialErosions = 0,
        initialDuststorms = 3,
      }
    }
  }
}

predefinedMapSettings.jungleWorld = {
  prettyName = "Jungle World",
  description = "Lots of small oceans. Lots of high yield plant bonuses. Few other bonuses.",
  config = {
    mapGeneratorConfig = {
      mapSizeSelection = 3,
      deltaFactor = 0.1,
      bonusTilesRatio = 16/10,
      totallyRandomTilesRatio = 0.05,
      tileEffects = {
        Plants = { weighting = 15, averageTileYield = 2.5, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 4, shapeFactor = 2, seedDistance = 4 },
        Titanium = { weighting = 1, averageTileYield = 1, yieldDiffusion = 0.5, maxYield = 2, seedPoints = 5, shapeFactor = 0.6, seedDistance = 4 },
        Steel = { weighting = 2, averageTileYield = 1.7, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 5, shapeFactor = 0.8, seedDistance = 4 },
        DrawCard = { weighting = 1, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 3, seedPoints = 5, shapeFactor = 0.2, seedDistance = 4 },
        Heat = { weighting = 0, averageTileYield = 2, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
        Energy = { weighting = 0, averageTileYield = 1.5, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 2, shapeFactor = 2, seedDistance = 4 },
        Credits = { weighting = 0, averageTileYield = 5, yieldDiffusion = 0.5, maxYield = 6, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 },
        OtherEffects = { weighting = 1, averageTileYield = 0.0, yieldDiffusion = 0.0, maxYield = 0, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 } },
      oceanSettings = {
        oceanSeedPoints = 8,
        oceanSeedMinDistance = 2,
        oceanShapeFactor = 0, -- values >= 1 --> bulky cluster, values < 1 --> snakey ocean lines
      },
      absoluteTiles = {
        volcanoTiles = 0,
        blockedTiles = 0, -- blocked tiles give adjancency bonuses (e.g. draw 1 card, gain 1 plant, etc)
        initialErosions = 0,
        initialDuststorms = 3,
      }
    }
  }
}

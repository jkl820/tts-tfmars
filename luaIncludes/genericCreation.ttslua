function toggleAntiLag(playerColor)
	local playerIndex = getPlayerIndexByColor(playerColor)
	if gameState.allPlayers[playerIndex].playerArea.playerAntiLagBoard ~= nil then
		getObjectFromGUID(gameState.allPlayers[playerIndex].playerArea.playerAntiLagBoard).destruct()
		gameState.allPlayers[playerIndex].playerArea.playerAntiLagBoard = nil
	else
		if Player[playerColor].seated then
			broadcastToColor("To use anti lag board just move cards from your hand there. It will lag less if enough players do this. They are hidden there from other players.", playerColor, {0,1,0})
		end
		createPlayerAntiLagBoard(gameState.allPlayers[playerIndex])
	end

  gameState.allPlayers[playerIndex].wasUpdated = true
end

function toggleOrg(playerColor)
  local player = getPlayerByColor(playerColor)
  if player.playerArea.playerOrgHelpBoard ~= nil then
  	getObjectFromGUID(player.playerArea.playerOrgHelpBoard).destruct()
  	player.playerArea.playerOrgHelpBoard = nil
  else
  	createPlayerOrgHelpBoard(player)
  end
end

function toggleIconTableaus(playerColor)
  local playerIndex = getPlayerIndexByColor(playerColor)
  local player = gameState.allPlayers[playerIndex]
  if player.playerArea.iconTableaus ~= nil and #player.playerArea.iconTableaus > 0 then
    for _, iconTableauGuid in pairs(player.playerArea.iconTableaus) do
      local iconTableau = getObjectFromGUID(iconTableauGuid)
      if iconTableau ~= nil then
        iconTableau.destruct()
      end
    end
    player.playerArea.iconTableaus = {}
  else
    createIconTableaus(player)
  end

  player.wasUpdated = true
end

function toggleActivationTableau(playerColor)
  local player = getPlayerByColor(playerColor)
  if player.playerArea.activationTableau ~= nil then
    local object = getObjectFromGUID(player.playerArea.activationTableau)

    player.playerArea.activationTableau = nil

    if object ~= nil then
      object.destruct()
    else
      createActivationTableau(player)
    end
  else
    createActivationTableau(player)
  end

  player.wasUpdated = true
end

function togglePersonalScoreBoard(playerColor)
  local player = getPlayerByColor(playerColor)
  if player.playerArea.personalScoreBoard ~= nil then
    local object = getObjectFromGUID(player.playerArea.personalScoreBoard)

    player.playerArea.personalScoreBoard = nil

    if object ~= nil then
      object.destruct()
    else
      createPersonalScoreBoard(player)
    end
  else
    createPersonalScoreBoard(player)
  end

  player.wasUpdated = true
end

function createTRCube(player)
	function coroutineCloneCube()
		-- Race condition avoidance
		coroutine.yield(0)
		local genericCube = gameObjectHelpers.getObjectByName("genericTRCube")
		local newCube = genericCube.clone()
		while newCube.getGUID() == genericCube.getGUID() do
			coroutine.yield(0)
		end
		newCube.setName(player.color .. " TR Cube")
		newCube.setLock(true)
		newCube.setColorTint(stringColorToRGB(player.color))
		player.playerArea.trCube = newCube.getGUID()
		-- Fix cubes
		updateCubePositionsOnTerraformingBar()
		coroutine.yield(0)
		coroutine.yield(0)
		updateCubePositionsOnTerraformingBar()
    player.wasUpdated = true
		return 1
	end
	startLuaCoroutine(self, "coroutineCloneCube")
end

function createPlayerBoard(player)
  log("Creating player board for "..player.color)

  local transform = tablePositions.player.playerSpawnPositions[player.color]
  local genericBoard = gameObjectHelpers.getObjectByName("genericPlayerBoard")
  if transform.isLeftRightFlipped then
    genericBoard = gameObjectHelpers.getObjectByName("genericPlayerBoardFlipped")
  end

  createClonableObject(
    genericBoard,
    transform.pos,
    transform.rot,
    function(clonedObjectGuid)
      player.playerArea.playerMat = genericBoard.getVar("lastClonedSelfGuid")
      local clonedBoard = getObjectFromGUID(player.playerArea.playerMat)

      clonedBoard.call("initialize", { playerColor = player.color, playerName = player.name })
      playerActionFuncs_toggleAutoPassOption({playerColor = player.color, delta = 0}) -- update button on playermat
      player.wasUpdated = true
    end
  )
end

function createPlayerAntiLagBoard(player)
  local genericBoard = 	gameObjectHelpers.getObjectByName("genericPlayerAntiLagBoard")
  local offsetTransform = tableHelpers.deepClone(getObjectFromGUID(player.playerArea.playerMat).call("getAntiLagBoardTransform"))

  createClonableObject(
    genericBoard,
    offsetTransform.pos,
    offsetTransform.rot,
    function(clonedObjectGuid)
      player.playerArea.playerAntiLagBoard = clonedObjectGuid
      local clonedBoard = getObjectFromGUID(player.playerArea.playerAntiLagBoard)

      clonedBoard.call("setPlayerColor", player.color)
      clonedBoard.call("activateBoard")
      player.wasUpdated = true
    end
  )
end

function createPlayerOrgHelpBoard(player)
  local genericBoard = 	gameObjectHelpers.getObjectByName("genericPlayerOrgHelpBoard")
  local offsetTransform = tableHelpers.deepClone(getObjectFromGUID(player.playerArea.playerMat).call("getOrgBoardTransform"))
  createClonableObject(
    genericBoard,
    offsetTransform.pos,
    offsetTransform.rot,
    function(clonedObjectGuid)
      player.playerArea.playerOrgHelpBoard = clonedObjectGuid
      local clonedBoard = getObjectFromGUID(player.playerArea.playerOrgHelpBoard)
      clonedBoard.setVar("playerColor", player.color)
      player.wasUpdated = true
    end
  )
end

function createIconTableaus(player)
  log("Creating icon tableau for "..player.color)
  function createIconTableausCoroutine()

    local tableau = gameObjectHelpers.getObjectByName("genericIconTableau")

    while player.playerArea == nil or getObjectFromGUID(player.playerArea.playerMat) == nil do
      coroutine.yield(0)
    end

    local offsetTransform = tableHelpers.deepClone(getObjectFromGUID(player.playerArea.playerMat).call("getIconTableuTransform"))

    createClonableObject(tableau,
      offsetTransform.pos,
  		offsetTransform.rot,
      function(clonedObjectGuid)
        if player.playerArea.iconTableaus == nil then
          player.playerArea.iconTableaus = {}
        end
        table.insert(player.playerArea.iconTableaus, clonedObjectGuid)
        local clonedIconTableau = getObjectFromGUID(clonedObjectGuid)
        clonedIconTableau.call("setPlayerColor", player.color)
      end)

    local offset = {-7.45,0,0}
    if gameState.activeExpansions.pathfinders then
      local basePathfinderTableau = gameObjectHelpers.getObjectByName("genericPathfinderIconTableau")
      local pathfinderOffset = vectorHelpers.addVectors(offsetTransform.pos, vectorHelpers.rotateVectorY(offset, offsetTransform.rot[2]))
      offset = vectorHelpers.addVectors(offset, {-1.45,0,0})

      createClonableObject(basePathfinderTableau,
        pathfinderOffset,
    		offsetTransform.rot,
        function(clonedObjectGuid)
          if player.playerArea.iconTableaus == nil then
            player.playerArea.iconTableaus = {}
          end
          table.insert(player.playerArea.iconTableaus, clonedObjectGuid)
          local clonedIconTableau = getObjectFromGUID(clonedObjectGuid)
          clonedIconTableau.call("setPlayerColor", player.color)
          player.wasUpdated = true
        end)
    end

    if gameState.activeExpansions.highOrbit then
      local highOrbitTableau = gameObjectHelpers.getObjectByName("genericHighOrbitIconTableau")
      local highOrbitOffset = vectorHelpers.addVectors(offsetTransform.pos, vectorHelpers.rotateVectorY(offset, offsetTransform.rot[2]))
      offset = vectorHelpers.addVectors(offset, {-1.45,0,0.0})

      createClonableObject(highOrbitTableau,
        highOrbitOffset,
        offsetTransform.rot,
        function(clonedObjectGuid)
          if player.playerArea.iconTableaus == nil then
            player.playerArea.iconTableaus = {}
          end
          table.insert(player.playerArea.iconTableaus, clonedObjectGuid)
          local clonedIconTableau = getObjectFromGUID(clonedObjectGuid)
          clonedIconTableau.call("setPlayerColor", player.color)
          player.wasUpdated = true
        end)
    end

    return 1
  end
  startLuaCoroutine(self, "createIconTableausCoroutine")
end

function createActivationTableau(player)
  function createActivationTableauCoroutine()
    local genericActivationTableau = gameObjectHelpers.getObjectByName("genericActivationTableau")

    while player.playerArea == nil or getObjectFromGUID(player.playerArea.playerMat) == nil do
      coroutine.yield(0)
    end

    local offsetTransform = tableHelpers.deepClone(getObjectFromGUID(player.playerArea.playerMat).call("getActivationTableuTransform"))
    createClonableObject(genericActivationTableau, offsetTransform.pos, offsetTransform.rot,
      function(clonedObjectGuid)
        player.playerArea.activationTableau = clonedObjectGuid
        local clonedActivationTableau = getObjectFromGUID(player.playerArea.activationTableau)
        clonedActivationTableau.call("setPlayerColor", player.color)
        player.wasUpdated = true
      end)

    return 1
  end
  startLuaCoroutine(self, "createActivationTableauCoroutine")
end

function createPersonalScoreBoard(player)
  if player.neutral then return end
  function cloneScoreBoardCoroutine()
    local playerMat = getObjectFromGUID(player.playerArea.playerMat)
    local targetTransform = tableHelpers.deepClone(getObjectFromGUID(player.playerArea.playerMat).call("getPersonalScoreBoardTransform"))
    local object = gameObjectHelpers.getObjectByName("genericScoreBoard")
    newObject = object.clone()
    newObject.lock()

    while(object.getGUID() == newObject.getGUID()) do
    		coroutine.yield(0)
    end

    for i=1, 5 do
      coroutine.yield(0)
    end

    local playerIndex = getPlayerIndexByColor(player.color)

    player.playerArea.personalScoreBoard = newObject.getGUID()
    newObject.setPositionSmooth(targetTransform.pos, false, false)
    newObject.setRotation(targetTransform.rot)
    newObject.setScale({1.45,1,1.45})
    newObject.call("setOwner", player.color)
    newObject.call("registerEventHandlers")
    newObject.call("setPlayerIndex", playerIndex)
    updatePlayerStats(playerIndex)

    return 1
  end
  startLuaCoroutine(self, "cloneScoreBoardCoroutine")
end

cloningOngoing = false

function isCloningOngoing()
  for guid, status in pairs(cloningOngoingTable) do
    if status then
      return true
    end
  end
  return false
end

cloningOngoingTable = {}
-- player = gameState.allPlayers[1]
-- createPlayerBoard(player)
function createClonableObject(object, targetPosition, targetRotation, callback, exclusiveCloningRequested)
	function coroutineCloneObject()
    local guid = object
    local clonableObject = nil

    if type(object) == "userdata" then
		    clonableObject = object
        guid = clonableObject.getGUID()
    else
		    clonableObject = getObjectFromGUID(object)
    end

		-- If scripts are not autoplay this can trigger a race condition if we don't yield
		coroutine.yield(0)

    if clonableObject.tag == "Bag" then
      exclusiveCloningRequested = true
    end

    -- Wait for the exclusive slot to be free
    while exclusiveCloningRequested and exclusiveCloning do
      coroutine.yield(0)
    end

    if exclusiveCloningRequested then
      exclusiveCloning = true
      while isCloningOngoing() do
        coroutine.yield(0)
      end
    end

		--log("Enter clone object coroutine")
		-- Make sure only one cloning is going on at one time
		while cloningOngoingTable[guid] or (exclusiveCloning and not exclusiveCloningRequested) do
			coroutine.yield(0)
		end
		cloningOngoingTable[guid] = true

		--log("Starting clonable object creation for object with guid "..clonableObject.getGUID())

    clonableObject.call("copySelf", {position = targetPosition,rotation = targetRotation})
		while clonableObject.getVar("lastClonedSelfGuid") == nil do
			coroutine.yield(0)
		end

    local clonedObjectGuid = clonableObject.getVar("lastClonedSelfGuid")

    if type(callback) == "function" then
      callback(clonedObjectGuid)
    end
		--log("Finished cloning object")
    if exclusiveCloningRequested then
      exclusiveCloning = false
    end

		cloningOngoingTable[guid] = false

		return 1
	end

  startLuaCoroutine(self, "coroutineCloneObject")
end

function fastCreateClonableObject(object, targetPosition, targetRotation, callback, exclusiveCloningRequested)
	function coroutineCloneObject()

    local guid = object
    local clonableObject = nil

    if type(object) == "userdata" then
		    clonableObject = object
        guid = clonableObject.getGUID()
    else
		    clonableObject = getObjectFromGUID(object)
    end

		-- Make sure only one cloning is going on at one time
		while cloningOngoingTable[guid] do
			coroutine.yield(0)
		end
		cloningOngoingTable[guid] = true

    clonableObject.call("copySelf", {position = targetPosition, rotation = targetRotation})
		while clonableObject.getVar("lastClonedSelfGuid") == nil do
			coroutine.yield(0)
		end

    local clonedObjectGuid = clonableObject.getVar("lastClonedSelfGuid")
    if type(callback) == "function" then
      callback(clonedObjectGuid)
    end

		cloningOngoingTable[guid] = false

		return 1
	end

  startLuaCoroutine(self, "coroutineCloneObject")
end

--
-- toBeClonedObject = {}
-- function toBeClonedObject:new(owningPlayer, position, rotation, genericToBeClonedObjectName, postHookFunction)
--   obj = {}
--   obj.owningPlayer = owningPlayer
--   obj.position = position
--   obj.rotation = rotation
--   obj.clonedObject = {}
--   obj.genericToBeClonedObjectName = genericToBeClonedObjectName
--   obj.hookFunc = postHookFunction
--   return obj
-- end
--
-- -- an example for implementation guideline:
-- -- example = toBeClonedObject:new(aPlayer, aPosition, aRotation, "myObject")
-- -- createClonedObject(example)
--
-- function cloneObject(toBeClonedObject)
--   print("Creating 'insertNameOfObjectHere' for "..toBeClonedObject.owningPlayer.color)
--
-- 	function coroutineCloneObject()
-- 		-- If scripts are not autoplay this can trigger a race condition if we don't yield
-- 		coroutine.yield(0)
-- 		-- Make sure only one cloning is going on at one time
-- 		while cloningOngoing do
-- 			coroutine.yield(0)
-- 		end
--
--     cloningOngoing = true
-- 		local genericObject = gameObjectHelpers.getObjectByName(toBeClonedObject.genericToBeClonedObjectName)
-- 		genericObject.call("instantiate", toBeClonedObject.position)
--
--     while genericObject.getVar("lastClonedSelfGuid") == nil do
-- 			coroutine.yield(0)
-- 		end
--
-- 		toBeClonedObject.clonedObject = genericBoard.getVar("lastClonedSelfGuid")
-- 		cloningOngoing = false
--     local clonedObject = getObjectFromGUID(toBeClonedObject.clonedObject)
--
--     clonedObject.call("setPlayerColor", toBeClonedObject.owningPlayer.color)
--
--     -- More race conditions
--     coroutine.yield(0)
--     clonedObject.call("setPlayerName", toBeClonedObject.owningPlayer.name)
--
--     hookFunction()
--
-- 		return 1
-- 	end
-- 	startLuaCoroutine(self, "coroutineCloneObject")
--  return toBeClonedObject.clonedObject
-- end

terraformingParametersMap = {}
terraformingParametersMap["Ocean"] = "Ocean"
terraformingParametersMap["Oceans"] = "Ocean"
terraformingParametersMap["Temp"] = "Temp"
terraformingParametersMap["Temperature"] = "Temp"
terraformingParametersMap["O2"] = "O2"
terraformingParametersMap["Oxygen"] = "O2"
terraformingParametersMap["TFVenus"] = "TFVenus"

requirementType = {
  globalParameter = "globalParameter",
  tags = "tags",
  delegate = "delegate",
  partyLeader = "partyLeader",
  chairman = "chairman",
  infrastructure = "infrastructure",
  party = "party",
  terraformingRating = "terraformingRating",
  production = "production",
  ownedObjectsInPlay = "ownedObjectsInPlay",
}

requirementTarget = {
  any = "any",
  own = "own",
  other = "other",
  otherSingle = "otherSingle",
  neutral = "neutral",
}

requirementTargetMap = {}
requirementTargetMap["Any"] = requirementTarget.any
requirementTargetMap["any"] = requirementTarget.any
requirementTargetMap["Own"] = requirementTarget.own
requirementTargetMap["own"] = requirementTarget.own
requirementTargetMap["Other"] = requirementTarget.other
requirementTargetMap["other"] = requirementTarget.other
requirementTargetMap["OtherSingle"] = requirementTarget.otherSingle
requirementTargetMap["otherSingle"] = requirementTarget.otherSingle
requirementTargetMap["Neutral"] = requirementTarget.neutral
requirementTargetMap["neutral"] = requirementTarget.neutral

rawRequirementInterpreter = {}
rawRequirementInterpreter.extras = { wildCardTagsAvailable = 0 }

rawRequirementInterpreter.resetExtrasForPlayer = function(tmPlayer)
  rawRequirementInterpreter.extras = { wildCardTagsAvailable = getPlayerTags({tagName = "WildCard", playerColor = tmPlayer.color}) }
end

rawRequirementInterpreter.evaluateRequirement = function(tmPlayer, key, value)
  local requirement = translateRawRequirement(key, value)
  if requirement == nil then
    logging.printToAll("Unknown requirement found - ignoring", {1,0.25,0}, {1,0,0}, loggingModes.exception)
    return true
  end

  local checkValue = getRequirementCheckAgainstValue(tmPlayer, requirement)

  local result = isRequirementFulfilled(tmPlayer, requirement, checkValue)

  return result
end

-- it's a hack, but it works for now
function getSpecificPlayerPropertiesRemotely(params)
  local requirement = translateRawRequirement(params.formula, 0)
  local tmPlayer = getPlayerByColor(params.playerColor)
  return getRequirementCheckAgainstValue(tmPlayer, requirement)
end

function translateRawRequirement(rawKey, rawValue)
  local target = nil
  local isMax = false
  local cleanedUpKey = rawKey

  if string.find(cleanedUpKey, "Max") then
    isMax = true
    cleanedUpKey = cleanedUpKey:gsub("Max", "")
  end

  for reqTargetString, translatedTarget in pairs(requirementTargetMap) do
    if string.find(cleanedUpKey, reqTargetString) then
      target = translatedTarget
      cleanedUpKey = cleanedUpKey:gsub(reqTargetString, "")
    end
  end

  if target == nil then
    target = requirementTarget.any
  end

  for _, func in pairs(requirementTranslationFunctions) do
    local requirement = func(cleanedUpKey, rawValue)
    if requirement ~= nil then
      requirement.isMax = isMax
      requirement.target = target
      return requirement
    end
  end

  return nil
end

function getRequirementCheckAgainstValue(tmPlayer, requirement)
  for _, func in pairs(getCheckAgainstValueFunctions) do
    local checkValue = func(tmPlayer, requirement)
    if checkValue ~= nil then
      return checkValue
    end
  end
end

function isRequirementFulfilled(tmPlayer, requirement, relevantValue)
  local reqModifiers = tmPlayer.reqModifiers

  local permanentModifier = reqModifiers.permanent[requirement.key]
  local transientModifier = reqModifiers.transient[requirement.key]

  local totalModifier = 0
  if permanentModifier ~= nil then
    totalModifier = totalModifier + permanentModifier
  end

  if transientModifier ~= nil then
    totalModifier = totalModifier + transientModifier
  end

  if requirement.type ~= requirementType.tags then
    if requirement.isMax then
      return relevantValue <= requirement.value + totalModifier
    else
      return relevantValue >= requirement.value - totalModifier
    end
  -- very special handling for tags needed (thank you very much WildCard tag...)
  else
    if requirement.isMax then
      return relevantValue <= requirement.value + totalModifier
    else
      local targetValue = requirement.value - totalModifier
      if relevantValue >= targetValue then
        return true
      elseif relevantValue + rawRequirementInterpreter.extras.wildCardTagsAvailable >= targetValue then
        rawRequirementInterpreter.extras.wildCardTagsAvailable = rawRequirementInterpreter.extras.wildCardTagsAvailable - (targetValue - relevantValue)
        return true
      else
        return false
      end
    end
  end
end

requirementTranslationFunctions = {}
requirementTranslationFunctions.tryTranslateGlobalParametersRequirement = function(inputKey, rawValue)
  for key, translatedKey in pairs(terraformingParametersMap) do
    if key == inputKey then
      local requirement = {}
      requirement.key = translatedKey
      requirement.type = requirementType.globalParameter
      local steps = nil
      if inputKey == "O2" then
        steps = globalParameters.oxygen.mappings[globalParameterSystem.values.oxygen.selection].steps
      elseif inputKey == "Temp" then
        steps = globalParameters.temperature.mappings[globalParameterSystem.values.temperature.selection].steps
      elseif inputKey == "TFVenus" then
        steps = globalParameters.venus.mappings[globalParameterSystem.values.venus.selection].steps
      elseif string.lower(inputKey) == "oceans" or string.lower(inputKey) == "ocean" then
        steps = globalParameters.ocean.mappings[globalParameterSystem.values.ocean.selection].steps
      end
      for stepIndex, entry in pairs(steps) do
        if rawValue == entry then
          requirement.value = stepIndex
        end
      end

      if requirement.value == nil then
        requirement.value = tableHelpers.findNearestMatch(steps, rawValue).index
      end

      return requirement
    end
  end
end

requirementTranslationFunctions.tryTranslateTagsRequirement = function(inputKey, rawValue)
  for _, tagCollection in pairs(icons) do
    for _, tag in ipairs(tagCollection) do
      if inputKey == tag then
        local requirement = {}
        requirement.value = rawValue
        requirement.type = requirementType.tags
        requirement.key = tag
        return requirement
      end
    end
  end
end

requirementTranslationFunctions.tryTranslateDelegateRequirement = function(inputKey, rawValue)
  if inputKey == "delegate" or inputKey == "Delegate" then
    local requirement = {}
    requirement.value = rawValue
    requirement.type = requirementType.delegate
    requirement.key = "delegate"
    return requirement
  end
end

requirementTranslationFunctions.tryTranslatePartyLeaderRequirement = function(inputKey, rawValue)
  if inputKey == "partyLeader" or inputKey == "PartyLeader" then
    local requirement = {}
    requirement.value = rawValue
    requirement.type = requirementType.partyLeader
    requirement.key = "partyLeader"
    return requirement
  end
end

requirementTranslationFunctions.tryTranslateChairmanRequirement = function(inputKey, rawValue)
  if inputKey == "chairman" or inputKey == "Chairman" then
    local requirement = {}
    requirement.value = rawValue
    requirement.type = requirementType.chairman
    requirement.key = "chairman"
    return requirement
  end
end

requirementTranslationFunctions.tryTranslateInfrastructureRequirement = function(inputKey, rawValue)
  if inputKey == "infrastructure" or inputKey == "Infrastructure" or inputKey == "SpaceBiggerThanInfrastructure" then
    local requirement = {}
    requirement.value = 1
    requirement.type = requirementType.infrastructure
    requirement.key = "infrastructure"
    return requirement
  end
end

requirementTranslationFunctions.tryTranslatePartyRequirement = function(inputKey, rawValue)
  for partyId, partyName in pairs(marsSenate.parties) do
    local requirement = {}
    if partyName == inputKey then
      if rawValue ~= nil then
        requirement.value = rawValue
      else
        requirement.value = 2
      end
      requirement.type = requirementType.party
      requirement.key = partyName
      return requirement
    end
  end
end

requirementTranslationFunctions.tryTranslateTagComparsionRequirement = function(inputKey, rawValue)
  for _, tagCollection in pairs(icons) do
    for _, tag in ipairs(tagCollection) do
      if inputKey == tag then
        local requirement = {}
        requirement.value = rawValue
        requirement.type = requirementType.tags
        requirement.key = tag
        return requirement
      end
    end
  end
end

requirementTranslationFunctions.tryTranslateTerraformingRatingRequirement = function(inputKey, rawValue)
  if inputKey == "TR" or inputKey == "tr" then
    local requirement = {}
    requirement.value = rawValue
    requirement.type = requirementType.terraformingRating
    requirement.key = "tr"
    return requirement
  end
end

requirementTranslationFunctions.tryTranslateProductionRequirement = function(inputKey, rawValue)
  for _, productionType in ipairs(resources.baseGame) do
    if string.lower(inputKey) == productionType then
      local requirement = {}
      requirement.value = rawValue
      requirement.type = requirementType.production
      requirement.key = productionType
      return requirement
    end
  end
end

requirementTranslationFunctions.tryTranslateOwnedObjectsRequirement = function(inputKey, rawValue)
  for _, expansion in pairs(ownableObjects) do
    if expansion.friendlyNameMapping ~= nil then
      for objectId, objectFriendlyNames in pairs(expansion.friendlyNameMapping) do
        for _, name in ipairs(objectFriendlyNames) do
          if name == inputKey then
            local requirement = {}
            requirement.value = rawValue
            requirement.type = requirementType.ownedObjectsInPlay
            requirement.key = objectId
            return requirement
          end
        end
      end
    end
  end
end

getCheckAgainstValueFunctions = {}

getCheckAgainstValueFunctions.getGlobalParameterValue = function(tmPlayer, requirement)
  if requirement.type == requirementType.globalParameter then
    if requirement.key == "TFVenus" then
      return globalParameterSystem.values.venus.stepIndex
    elseif requirement.key == "O2" then
      return globalParameterSystem.values.oxygen.stepIndex
    elseif requirement.key == "Temp" then
      return globalParameterSystem.values.temperature.stepIndex
    elseif requirement.key == "Ocean" then
      return globalParameterSystem.values.ocean.stepIndex + globalParameterSystem.values.ocean.extra
    end
  end
end

getCheckAgainstValueFunctions.getTagCounts = function(tmPlayer, requirement)
  if requirement.type == requirementType.tags then
    if requirement.target == requirementTarget.own then
      return getPlayerTags({tagName = requirement.key, playerColor = tmPlayer.color})
    elseif requirement.target == requirementTarget.other then
      return getAllOtherPlayersTags({tagName = requirement.key, playerColor = tmPlayer.color})
    else
      local tagCount = getPlayerTags({tagName = requirement.key, playerColor = tmPlayer.color})
      return tagCount + getAllOtherPlayersTags({tagName = requirement.key, playerColor = tmPlayer.color})
    end
  end
end

getCheckAgainstValueFunctions.getTerraformingCount = function(tmPlayer, requirement)
  if requirement.type == requirementType.terraformingRating then
    local result = 0
    for i, player in ipairs(gameState.allPlayers) do
      if requirement.target == requirementTarget.own then
        result = tmPlayer.terraformingRating
      elseif requirement.target == requirementTarget.other then
        if player ~= tmPlayer then
          result = result + player.terraformingRating
        end
      elseif requirement.target == requirementTarget.otherSingle then
        if player ~= tmPlayer and player.terraformingRating > result then
          result = player.terraformingRating
        end
      elseif requirement.target == requirementTarget.any then
        result = result + player.terraformingRating
      end
    end
    return result
  end
end

-- delegates requirements are a pain in the ass
getCheckAgainstValueFunctions.getActiveDelegates = function(tmPlayer, requirement)
  if requirement.type == requirementType.delegate then
    -- not needed yet
    return 999
  elseif requirement.type == requirementType.partyLeader then
    local partyLeaderCount = 0

    for _, info in pairs(gameState.turmoilData.parties) do
      if requirement.target == "own" and getPartyLeadColor(info) == tmPlayer.color then
        partyLeaderCount = partyLeaderCount + 1
      end
    end

    return partyLeaderCount
  elseif requirement.type == requirementType.chairman then
    local chairmanColor = getChairman().getDescription()

    if requirement.target == "neutral" and chairmanColor == "Neutral" then
      return 1
    elseif requirement.target == "own" and tmPlayer.color == chairmanColor then
      return 1
    end

    return 0
  end
end

getCheckAgainstValueFunctions.getSpaceAgainstInfrastructureValue = function(tmPlayer, requirement)
  if requirement.type == requirementType.infrastructure then
    local spaceTags = getPlayerTags({tagName = "Space", playerColor = tmPlayer.color})
    spaceTags = spaceTags + getPlayerTags({tagName = "WildCard", playerColor = tmPlayer.color})
    local infrastructureTags = getPlayerTags({tagName = "Infrastructure", playerColor = tmPlayer.color})

    if spaceTags > infrastructureTags then
      return 1
    else
      return 0
    end
  end
end

getCheckAgainstValueFunctions.getPoliticalPartyInfluence = function(tmPlayer, requirement)
  if requirement.type == requirementType.party then
    if requirement.key == getRulingParty().partyId then
      return 999
    else
      return playersPartyDelegateCount(tmPlayer.color, requirement.key)
    end
  end
end

getCheckAgainstValueFunctions.getProductionValue = function(tmPlayer, requirement)
  if requirement.type == requirementType.production then
    local productionValue = 0
    for i, player in ipairs(gameState.allPlayers) do
      if requirement.target == requirementTarget.own then
        productionValue = Global.call("getPlayerProduction", { resourceType=requirement.key, playerColor = tmPlayer.color })
      elseif requirement.target == requirementTarget.other then
        if player ~= tmPlayer then
          productionValue = productionValue + Global.call("getPlayerProduction", { resourceType=requirement.key, playerColor = player.color })
        end
      elseif requirement.target == requirementTarget.otherSingle then
        if player ~= tmPlayer and player.ownedObjects[requirement.key] > productionValue then
          productionValue = productionValue + Global.call("getPlayerProduction", { resourceType=requirement.key, playerColor = player.color })
        end
      elseif requirement.target == requirementTarget.any then
        productionValue = productionValue + Global.call("getPlayerProduction", { resourceType=requirement.key, playerColor = player.color })
      end
    end
    return productionValue
  end
end

getCheckAgainstValueFunctions.getPlayerOwnedObjectsInPlay = function(tmPlayer, requirement)
  if requirement.type == requirementType.ownedObjectsInPlay then
    local objectsCount = 0
    for i, player in ipairs(gameState.allPlayers) do
      if requirement.target == requirementTarget.own then
        objectsCount = tmPlayer.ownedObjects[requirement.key]
      elseif requirement.target == requirementTarget.other then
        if player ~= tmPlayer then
          objectsCount = objectsCount + player.ownedObjects[requirement.key]
        end
      elseif requirement.target == requirementTarget.otherSingle then
        if player ~= tmPlayer and player.ownedObjects[requirement.key] > objectsCount then
          objectsCount = player.ownedObjects[requirement.key]
        end
      elseif requirement.target == requirementTarget.any then
        objectsCount = objectsCount + player.ownedObjects[requirement.key]
      end
    end
    return objectsCount
  end
end

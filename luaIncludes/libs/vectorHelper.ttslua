vectorHelpers = vectorHelpers or {}

vectorHelpers.addVectors = function(v1, v2)
	return {
		(v1[1] or v1.x) + (v2[1] or v2.x),
		(v1[2] or v1.y) + (v2[2] or v2.y),
		(v1[3] or v1.z) + (v2[3] or v2.z)
	}
end

vectorHelpers.subtractVectors = function(v1, v2)
	return {
		(v1[1] or v1.x) - (v2[1] or v2.x),
		(v1[2] or v1.y) - (v2[2] or v2.y),
		(v1[3] or v1.z) - (v2[3] or v2.z)
	}
end

vectorHelpers.scaleVector = function(v1, scale)
	return {
		(v1[1] or v1.x)*scale,
		(v1[2] or v1.y)*scale,
		(v1[3] or v1.z)*scale
	}
end

vectorHelpers.scaleVectorByVector = function(v1, v2)
	return {
		(v1[1] or v1.x) * (v2[1] or v2.x),
		(v1[2] or v1.y) * (v2[2] or v2.y),
		(v1[3] or v1.z) * (v2[3] or v2.z)
	}
end

vectorHelpers.divideVectorByVector = function(v1, v2)
	return {
		(v1[1] or v1.x) / (v2[1] or v2.x),
		(v1[2] or v1.y) / (v2[2] or v2.y),
		(v1[3] or v1.z) / (v2[3] or v2.z)
	}
end

vectorHelpers.vectorMagnitude = function(v1)
	return math.sqrt( v1[1]*v1[1] + v1[2]*v1[2] + v1[3]*v1[3] )
end

vectorHelpers.rotateVectorY = function(vector, rotateAngle)
	local cos = math.cos(rotateAngle * math.pi/180)
	local sin = math.sin(rotateAngle * math.pi/180)
	local resultVector = {}
	resultVector[1] = cos*vector[1] + sin*vector[3]
	resultVector[2] = vector[2]
	resultVector[3] = -sin*vector[1] + cos*vector[3]
	return resultVector
end

vectorHelpers.multiplyVectorWithScalar = function(vector, scalar)
  return {vector[1]*scalar, vector[2]*scalar, vector[3]*scalar}
end

vectorHelpers.print = function(v)
  Global.call("logging_printToAll", {
    message = "{" .. (v[1] or v.x) .. ", " .. (v[2] or v.y) .. "," ..(v[3] or v.z) .. "}",
    messageColor = {1,1,1},
    loggingMode = "unimportant",
  })
end

vectorHelpers.fromLocalToWorld = function(gameObject, localDeltaVector, flip)
  if flip then localDeltaVector[1] = -localDeltaVector[1] end
  localDeltaVector = vectorHelpers.rotateVectorY(localDeltaVector, gameObject.getRotation()[2])

  local scaledDeltaVector = vectorHelpers.scaleVectorByVector(localDeltaVector, gameObject.getScale())

  vector = vectorHelpers.addVectors(gameObject.getPosition(), scaledDeltaVector)

  return vector
end

vectorHelpers.fromWorldToLocal = function(gameObject, worldPosition, flip)
  local localUnscaledUnrotatedVector = vectorHelpers.subtractVectors(worldPosition, gameObject.getPosition())

  local downScale = {1/gameObject.getScale()[1], 1/gameObject.getScale()[2], 1/gameObject.getScale()[3]}
  local localScaledUnrotatedVector = vectorHelpers.scaleVectorByVector(localUnscaledUnrotatedVector, downScale)

  vector = vectorHelpers.rotateVectorY(localScaledUnrotatedVector, -gameObject.getRotation()[2])
  if flip then vector[1] = -vector[1] end
  return vector
end

vectorHelpers.truncateVectorEntries = function(inputVector, keepDecimals)
  local factor = 1
  for i=1, keepDecimals do
    factor = factor * 10
  end
  local tmpVec = vectorHelpers.scaleVector(inputVector, factor)

  tmpVec = {math.floor(tmpVec[1]), math.floor(tmpVec[2]), math.floor(tmpVec[3])}

  return vectorHelpers.scaleVector(tmpVec, 1/factor)
end

vectorHelpers.isInsideRange = function(targetPosition, testPosition, radius)
  local deltaVector = vectorHelpers.subtractVectors(targetPosition, testPosition)
  return vectorHelpers.vectorMagnitude(deltaVector) <= radius
end

#include ../staticData/gameSetup/gameConfigMatViews
#include gameConfigFunctions
#include gameConfigButtonFunctions
#include gameConfigMapGeneratorFunctions
#include gameConfigAwardsAndMilestones
#include gameConfigDraftingFunctions
#include gameConfigUpgrades

disableMarker = false -- used to mark buttons that have no effect

-- If you add new game config settings or change existing ones you need to:
-- 1. game config increment version
-- 2. write a upgrade function (gameConfigUpgrades.ttslua) that upgrades old configs to the updated version
-- Rest is handled during loading of the config.
-- Without that older configs from players will break --> lots of bug reports
gameConfig = {}
gameConfig.version = 2
gameConfig.mapSizeSelection = 3
gameConfig.setup = {
	solarPhase = false,
	classicBoard = false,
	-- Enabled Expansion
	prelude = true,
	colonies = false,
	venus = true,
	turmoil = false,
	corpEra = true,
	promos = true,
	drafting = false,
  extendedScripting = true,
  randomizer = false,
  -- Enabled Fan Content
  xenosCorps = false,
  fanMadeProjects = false,
  pathfinders = false,
  timer = false,
  highOrbit = false,
  venusPhaseTwo = false,
  ares = false,
  showFanMadeMaps = true,
  corpsToDraw = 2,
  randomMap = false,
  mapPredefinedSettings = 1,
  venusWin = false,
}
gameConfig.banList = { bannedCards = {}}
gameConfig.drafting = {}
gameConfig.drafting.initialDraftingSelection = 1
gameConfig.drafting.presetDraftingRule = draftingData.initialResearchPhase.freeDraft
gameConfig.drafting.custom = {
  active = false,
  selectedStep = 1,
  selectedSubStep = 1,
  considerForPayment = 10,
  steps =
  {
    {
      cardsToDeal =
      {
        corps = { amount = 2, targetHandIndex = 1 },
        preludes = { amount = 4, targetHandIndex = 3 },
        projects = { amount = 10, targetHandIndex = 2 },
      },
      subSteps = { { projects = 8, corps = 0, preludes = 0},
        { projects = 6, corps = 0, preludes = 0 },
        { projects = 4, corps = 0, preludes = 0 },
        { projects = 2, corps = 0, preludes = 0 }
      },
      directionOverride = 1
    }
  }
}
gameConfig.mapGeneratorConfig = {}
gameConfig.mapGeneratorConfig.mapSizeSelection = 3
gameConfig.mapGeneratorConfig.deltaFactor = 0.1
gameConfig.mapGeneratorConfig.bonusTilesRatio = 8/10
gameConfig.mapGeneratorConfig.totallyRandomTilesRatio = 0.1 -- percentage of regular tiles that will be rolled 50/50 empty vs bonus tiles, 10% by default
gameConfig.mapGeneratorConfig.tileEffects = {
  Plants = { weighting = 10, averageTileYield = 1.8, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
  Titanium = { weighting = 2, averageTileYield = 1, yieldDiffusion = 0.4, maxYield = 2, seedPoints = 5, shapeFactor = 0.6, seedDistance = 4 },
  Steel = { weighting = 5, averageTileYield = 1.7, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 5, shapeFactor = 0.8, seedDistance = 4 },
  DrawCard = { weighting = 3, averageTileYield = 1.45, yieldDiffusion = 0.25, maxYield = 3, seedPoints = 200, shapeFactor = 0.2, seedDistance = 4 },
  Heat = { weighting = 0, averageTileYield = 2, yieldDiffusion = 0.5, maxYield = 4, seedPoints = 3, shapeFactor = 2, seedDistance = 4 },
  Energy = { weighting = 0, averageTileYield = 1.5, yieldDiffusion = 0.25, maxYield = 4, seedPoints = 2, shapeFactor = 2, seedDistance = 4 },
  Credits = { weighting = 0, averageTileYield = 5, yieldDiffusion = 0.5, maxYield = 6, seedPoints = 5, shapeFactor = 0.2, seedDistance = 1 },
  OtherEffects = { weighting = 0, averageTileYield = 0.0, yieldDiffusion = 0.0, maxYield = 0, seedPoints = 200, shapeFactor = 0.2, seedDistance = 1 }
}
gameConfig.mapGeneratorConfig.oceanSettings = {
  oceanSeedPoints = 4,
  oceanSeedMinDistance = 4,
  oceanShapeFactor = 1, -- values >= 1 --> bulky cluster, values < 1 --> snakey ocean lines
}
gameConfig.mapGeneratorConfig.absoluteTiles = {
  volcanoTiles = 3,
  blockedTiles = 0, -- blocked tiles give adjancency bonuses (e.g. draw 1 card, gain 1 plant, etc)
  initialErosions = 0,
  initialDuststorms = 3,
}
gameConfig.milestones = {}
gameConfig.milestones.currentSelection = 1
gameConfig.milestones.randomizer = {}
gameConfig.milestones.randomizer.enabled = false
gameConfig.milestones.randomizer.numberOfMilestones = 5
gameConfig.milestones.randomizer.maxMilestonesPerCategory = 2
gameConfig.milestones.randomizer.guranteeHoverlord = true
gameConfig.milestones.randomizer.maxClaims = 3
gameConfig.awards = {}
gameConfig.awards.currentSelection = 1
gameConfig.awards.randomizer = {}
gameConfig.awards.randomizer.enabled = false
gameConfig.awards.randomizer.numberOfAwards = 5
gameConfig.awards.randomizer.maxAwardsPerCategory = 2
gameConfig.awards.randomizer.guranteeVenuphile = true
gameConfig.awards.randomizer.maxFunders = 3
gameConfig.globalParameters = {}
gameConfig.globalParameters.temperature = {
  selection = 1,
  bonusSelection = 1,
  aresSelection = 1,
}
gameConfig.globalParameters.oxygen = {
  selection = 1,
  bonusSelection = 1,
  aresSelection = 1,
}
gameConfig.globalParameters.ocean = {
  selection = 1,
  bonusSelection = 1,
  aresSelection = 1,
}
gameConfig.globalParameters.venus = {
  selection = 1,
  bonusSelection = 1,
  aresSelection = 1, -- just a dummy value for symmetry reasons - venus should never have an effect on Mars hazards
}
gameConfig.timerConfiguration = {
  isPaused = false,
  secondsPlusPerGeneration = 120,
  secondsPlusPerEndTurn = 15,
  secondsInitial = 1200,
  secondsPerNegativeVp = 10, -- minimum
  negativeVpThreshold = 0,
  delta = 1,
  pauseOnDraft = false,
  timeoutAction = "doNothing",
}

gameConfigFunctions = {}
gameConfigFunctions.refreshView = function()
  if gameState.setupIsDone and gameConfigMatViews.currentView < 3 then
    gameConfigMatViews.currentView = 3

    updateGameConfigSettingsView()
  end
end

function changeView(newView)
  local gameConfigMat = gameObjectHelpers.getObjectByName("gameConfigTile")

  if gameConfigMat.getButtons() ~= nil then
    for i=#gameConfigMat.getButtons()-1,0,-1 do
      gameConfigMat.removeButton(i)
    end
  end

  local buttons = tableHelpers.combineSingleValueTables({gameConfigMatViews.mainButtons, newView.buttons})

  createSetupButtonsInternal(gameConfigMat, buttons)
end

function createAllSetupButtons()
  local gameConfigMat = gameObjectHelpers.getObjectByName("gameConfigTile")

  local currentView = gameConfigMatViews.views[gameConfigMatViews.currentView]
  local buttons = tableHelpers.combineSingleValueTables({gameConfigMatViews.mainButtons, currentView.buttons})

  createSetupButtonsInternal(gameConfigMat, buttons)
end

function createSetupButtonsInternal(parentObject, buttons)
  local createFromScratch = parentObject.getButtons() == nil
  local buttonAmount = 0
  for i=1,#buttons do
    buttonFunctions.adaptButtonColor(buttons[i])
    buttonFunctions.adaptButtonLabel(buttons[i])
    buttonFunctions.adaptButtonTooltip(buttons[i])

    buttons[i].index = buttonAmount

    if transientState.finishSetupButtonDisabled and buttons[i].buttonType == "finishSetupButton" then
      buttons[i].color = {50/255,50/255,50/255,0.95}
    end

    if gameState.setupIsDone and buttons[i].buttonType == "startGameButton" then
      buttons[i].color = {255/255,115/255,0,0.95}
    end

    if createFromScratch then
      parentObject.createButton(buttons[i])
    else
      parentObject.editButton(buttons[i])
    end

    buttonAmount = buttonAmount + 1
  end
end

function printConfig(_,playerColor,_)
  Global.call("gameConfigFunctions_publishConfig")
end

function loadConfig(_,playerColor,_)
  local notes = getNotes()
  if string.find(notes, "{") and string.find(notes, "}") then
    Global.call("gameConfigFunctions_loadConfig")
  else
    broadcastToAll("No game config to load.")
  end
end

function clearNotes(_,playerColor,_)
  setNotes("")
end

function toggleGameConfigSettings(_,_,altClick)
  local delta = 1
  if altClick then
    delta = -1
  end

  gameConfigMatViews.currentView = gameConfigMatViews.currentView + delta
  updateGameConfigSettingsView()
end

function updateGameConfigSettingsView()
  if gameState.setupIsDone then
    if gameConfigMatViews.currentView > #gameConfigMatViews.views then
      gameConfigMatViews.currentView = 3
    elseif gameConfigMatViews.currentView < 3 then
      gameConfigMatViews.currentView = #gameConfigMatViews.views
    end
  else
    if gameConfigMatViews.currentView > #gameConfigMatViews.views then
      gameConfigMatViews.currentView = 1
    elseif gameConfigMatViews.currentView < 1 then
      gameConfigMatViews.currentView = #gameConfigMatViews.views
    end
  end

  local gameConfigMat = gameObjectHelpers.getObjectByName("gameConfigTile")
  local customization = {}
  customization.image = gameConfigMatViews.views[gameConfigMatViews.currentView].imageUrl
  gameConfigMat.setCustomObject(customization)
  local reloadedTile = gameConfigMat.reload()

  for _, buttonInfo in pairs(gameConfigMatViews.mainButtons) do
    if buttonInfo.buttonType == "toggleGameConfigSettings" then
      buttonInfo.label = gameConfigMatViews.views[gameConfigMatViews.currentView].toggleLabel
    end
  end

  Wait.frames(|| changeView(gameConfigMatViews.views[gameConfigMatViews.currentView]), 1)
end

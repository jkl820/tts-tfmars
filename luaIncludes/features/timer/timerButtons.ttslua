timerButtons = {}
timerButtons.buttonInfos = {
  {
    click_function = 'timerButtons_pauseUnpauseTimer',
    label = 'Pause timer',
    function_owner = Global,
    position = {0.00, 0.15, -0.3},
    rotation = {0, 0, 0},
    width = 2500,
    height = 500,
    font_size = 200,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    id = "isPaused" },
  {
    click_function = 'timerButtons_changeSecondsPlusPerGeneration',
    label = 'End Generation: 120s',
    baseLabel = "End Generation: ",
    function_owner = Global,
    position = {-1.4, 0.15, 0.00},
    rotation = {0, 0, 0},
    width = 2500,
    height = 500,
    font_size = 200,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    id = "secondsPlusPerGeneration" },
  {
    click_function = 'timerButtons_changeSecondsPerEndTurn',
    label = 'End Turn: 15s',
    baseLabel = "End Turn: ",
    function_owner = Global,
    position = {0, 0.15, 0.00},
    rotation = {0, 0, 0},
    width = 2500,
    height = 500,
    font_size = 200,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    id = "secondsPlusPerEndTurn" },
  {
    click_function = 'timerButtons_changeSecondsInitial',
    label = 'Starting Time: 1200s',
    baseLabel = "Starting Time: ",
    function_owner = Global,
    position = {1.4, 0.15, 0.00},
    rotation = {0, 0, 0},
    width = 2500,
    height = 500,
    font_size = 200,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    id = "secondsInitial" },
  {
    click_function = 'timerButtons_changeNegativeVpThreshold',
    label = '',
    tooltip = "Only has an effect with timeout action 'Negative VPs'",
    function_owner = Global,
    position = {1.4, 0.15, 0.30},
    rotation = {0, 0, 0},
    width = 2500,
    height = 500,
    font_size = 200,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    dynamicLabel = {prefix = "Negative VP Threshold:\n",
      value = {"transientState.timerConfiguration.negativeVpThreshold","gameConfig.timerConfiguration.negativeVpThreshold"},
      suffix = "s"}},
  {
    click_function = 'timerButtons_changeSecondsPerNegativeVp',
    label = '',
    tooltip = "Only has an effect with timeout action 'Negative VPs'",
    function_owner = Global,
    position = {0.00, 0.15, 0.30},
    rotation = {0, 0, 0},
    width = 2500,
    height = 500,
    font_size = 200,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    dynamicLabel = {prefix = "Seconds Per Negative VP:\n",
      value = {"transientState.timerConfiguration.secondsPerNegativeVp","gameConfig.timerConfiguration.secondsPerNegativeVp"},
      suffix = "s"}},
  {
    click_function = 'timerButtons_changeTimeoutAction',
    label = 'On Time Elapsed: Do nothing',
    function_owner = Global,
    position = {1.4, 0.15, -0.3},
    rotation = {0, 0, 0},
    width = 2500,
    height = 500,
    font_size = 175,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    id = "timeoutAction" },
  {
    click_function = 'timerButtons_changeFactor',
    label = '1',
    baseLabel = "",
    tooltip = "Amount of seconds added/subtracted when modifying timer values.",
    function_owner = Global,
    position = {-1.6, 0.15, -0.75},
    rotation = {0, 0, 0},
    width = 850,
    height = 500,
    font_size = 300,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    id = "delta" },
  {
    click_function = 'timerButtons_togglePauseOnDraft',
    label = 'Pause on draft',
    baseLabel = "",
    tooltip = "",
    function_owner = Global,
    position = {-1.4, 0.15, -0.3},
    rotation = {0, 0, 0},
    width = 2500,
    height = 500,
    font_size = 200,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
    onIndex = "gameState.timerConfiguration.pauseOnDraft" },
  {
    click_function = "noOperation",
    label = "i",
    tooltip = "Remarks about the timer settings:\n"..
      "- Sum of 'End Generation' seconds and six times 'End Turn' seconds must add up to at least 120 seconds.\n"..
      "    This shall avoid situations where players are completely knocked out of the game.\n"..
      "- Timer starts counting for the first player as soon as 'Start Nth generation' is pressed.\n"..
      "- After the game has started changes to the timer configuration will only apply with the start of the next generation.\n"..
      "    This shall avoid abuse of the timer settings tile.",
    function_owner = Global,
    position = {-2.1, 0.15, -0.75},
    rotation = {0, 0, 0},
    width = 500,
    height = 500,
    font_size = 420,
    scale = {0.2, 1, 0.2},
    color = {1,155/255,25/255},
  }
}

timerButtons.updateButtons = function()
  local timerTile = gameObjectHelpers.getObjectByName("timerConfigTile")
  local fromScratch = timerTile.getButtons() == nil
  local buttonAmount = 0
  local timerConfiguration = timerFunctions.getTimerConfiguration()

  for i, buttonInfo in pairs(timerButtons.buttonInfos) do
    buttonInfo.index = buttonAmount
    if buttonInfo.id == "timeoutAction" then
      if timerConfiguration.timeoutAction == "doNothing" then
        buttonInfo.label = "Timeout Action: Do Nothing"
        buttonInfo.tooltip = "Nothing will happen if time runs out for a player.\n"..
          "Time will continue ticking away into negative."
      elseif timerConfiguration.timeoutAction == "endTurn" then
        buttonInfo.label = "Timeout Action: End Turn"
        buttonInfo.tooltip = "If time runs out a player automatically ends their turn.\n"..
          "If added time from 'End Turn' is 0 then that player will pass for the remaining generation.\n"
      elseif timerConfiguration.timeoutAction == "giveNegativeVPs" then
        buttonInfo.label = "Timeout Action: Negative VPs"
        buttonInfo.tooltip = "Get negative VPs at the end of the game if your timer is below the threshold.\n"
      end
    elseif buttonInfo.id == "isPaused" then
      if timerConfiguration.isPaused then
        buttonInfo.label = "Timer is currently paused"
      else
        buttonInfo.label = "Click to pause the timer"
      end
    else
      for settingName, value in pairs(timerConfiguration) do
        if settingName == buttonInfo.id then
          buttonInfo.label = buttonInfo.baseLabel..value.."s"
        end
      end
    end

    if fromScratch then
      timerTile.createButton(buttonInfo)
    else
      timerTile.editButton(buttonInfo)
    end

    buttonAmount = buttonAmount + 1
  end

  buttonFunctions.createButtons(timerTile, timerButtons.buttonInfos)
end

function timerButtons_pauseUnpauseTimer(obj, playerColor, altClick)
  timerFunctions.pauseUnpauseTimer()
  timerButtons.updateButtons()
end

function timerButtons_changeSecondsPlusPerGeneration(obj, playerColor, altClick)
  local sign = altClick and -1 or 1
  timerFunctions.changeConfigValue("secondsPlusPerGeneration", sign, 0, 3600)
  timerButtons.updateButtons()
end

function timerButtons_changeSecondsPerEndTurn(obj, playerColor, altClick)
  local sign = altClick and -1 or 1
  timerFunctions.changeConfigValue("secondsPlusPerEndTurn", sign, 0, 300)
  timerButtons.updateButtons()
end

function timerButtons_changeSecondsInitial(obj, playerColor, altClick)
  local sign = altClick and -1 or 1
  timerFunctions.changeConfigValue("secondsInitial", sign, 120, 36000)
  timerButtons.updateButtons()
end

function timerButtons_changeFactor(obj, playerColor, altClick)
  timerFunctions.changeFactor(altClick)
  timerButtons.updateButtons()
end

function timerButtons_changeTimeoutAction(obj, playerColor, altClick)
  local sign = altClick and -1 or 1
  timerFunctions.toggleTimeoutAction(sign)
  timerButtons.updateButtons()
end

function timerButtons_togglePauseOnDraft(_,_,_)
  local timerConfiguration = timerFunctions.getTimerConfiguration()
  timerConfiguration.pauseOnDraft = not timerConfiguration.pauseOnDraft
  if gameState.timerConfiguration == nil then
    gameState.timerConfiguration = {}
  end
  gameState.timerConfiguration.pauseOnDraft = timerConfiguration.pauseOnDraft
  timerButtons.updateButtons()
end

function timerButtons_changeSecondsPerNegativeVp(_,_,altClick)
  local sign = altClick and -1 or 1
  timerFunctions.changeConfigValue("secondsPerNegativeVp", sign, 10, 600)
  timerButtons.updateButtons()
end

function timerButtons_changeNegativeVpThreshold(_,_,altClick)
  local sign = altClick and -1 or 1
  timerFunctions.changeConfigValue("negativeVpThreshold", sign, -3600, 0)
  timerButtons.updateButtons()
end

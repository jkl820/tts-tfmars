#include specialMilestones
#include ../staticData/milestoneData

function milestoneSystem_unclaimMilestone(params)
  return milestoneSystem.unclaimMilestone(params.milestoneIndex)
end

function milestoneSystem_toggleMilestones(params)
  milestoneSystem.changeMilestoneSet(params.nextSet)
end

milestoneSystem = {}
milestoneSystem.milestoneTilesInitialized = false
milestoneSystem.initialize = function()
  milestoneSystem.setupMilestones()
  Wait.time(function()
    if gameConfig.milestones.randomizer.enabled then
      milestoneSystem.randomize()
    else
      milestoneSystem.changeMilestoneSet(1)
    end
  end, 5)
end

milestoneSystem.tryClaimMilestone = function(playerColor, milestoneDescription, milestoneName, milestonePosition, tileGuid)
  local requirements = descriptionInterpreter.getKeyValuePairsFromInput(milestoneDescription, "Reqs:")
  local tmPlayer = getPlayerByColor(playerColor)

  if #gameState.claimedMilestones == gameState.milestones.maxClaims then
    logging.printToColor("You cannot claim "..milestoneName..". "..gameState.milestones.maxClaims.." milestones have been already claimed.", playerColor, playerColor, loggingModes.important)
    return false
  end

  -- special pathfinder's Chimera handling, fan mods always have such great ideas...
  local hasChimeraCorp = false
  for _, card in pairs(tmPlayer.ownedCards.Corp) do
    if card.guid == "e91b19" then
      tmPlayer.tagSystem.tagCounts["WildCard"] = tmPlayer.tagSystem.tagCounts["WildCard"] - 1
      hasChimeraCorp = true
    end
  end

  if next(requirements) == nil then
    if not specialMilestones.evaluateMilestone(tmPlayer, milestoneName) then
      return false
    end
  end

  local isAllowed = milestoneSystem.checkPrerequisites(tmPlayer, requirements)

  if isAllowed then
    milestoneSystem.claimMilestone(tmPlayer, milestoneName, milestonePosition, tileGuid)
  end

  -- special pathfinder's Chimera handling, fan mods always have such great ideas...
  if hasChimeraCorp then
    tmPlayer.tagSystem.tagCounts["WildCard"] = tmPlayer.tagSystem.tagCounts["WildCard"] + 1
  end

  return isAllowed
end

milestoneSystem.checkPrerequisites = function(tmPlayer, requirements)
  local requirementsFulfilled = objectActivationSystem.objectRequirementsFulfilled(tmPlayer, { rawRequirements = requirements })

  if requirementsFulfilled then
    local activationEffects = { resourceValues = { Credits = -8 } }

    local canAfford = objectActivationSystem_doAction({playerColor = tmPlayer.color,
      sourceName = "claiming a milestone",
      activationEffects = activationEffects})

    if canAfford then
      return true
    end
  end

  return false
end

milestoneSystem.setupMilestones = function(override)
  function setupMilestonesCoroutine()
    while transientState.setupMilestonesOngoing do
      coroutine.yield(0)
    end

    transientState.setupMilestonesOngoing = true

    if gameState.milestoneGuids ~= nil then
      for _, guid in pairs (gameState.milestoneGuids) do
        local obj = getObjectFromGUID(guid)
        if obj ~= nil then
          obj.destruct()
        end
      end
    end

    gameState.milestoneGuids = {}

    local milestonePlate = gameObjectHelpers.getObjectByName("milestonePlate")
    milestonePlate.call("resetSpawnPositions")

    local toSpawn = gameConfig.milestones.randomizer.enabled and gameConfig.milestones.randomizer.numberOfMilestones or 5
    if override ~= nil then toSpawn = override end
    for i=1, toSpawn do
      milestoneSystem.spawnMilestoneTile()
    end

    while #gameState.milestoneGuids < toSpawn do
      coroutine.yield(0)
    end

    for i=1, 30 do
      coroutine.yield(0)
    end

    transientState.setupMilestonesOngoing = false

    return 1
  end

  startLuaCoroutine(self, "setupMilestonesCoroutine")
end

milestoneSystem.randomize = function()
  function randomizeCoroutine()
    while transientState.setupMilestonesOngoing or transientState.milestoneRandomizerWorking do
      coroutine.yield(0)
    end

    transientState.milestoneRandomizerWorking = true
    transientState.latestMilestones = tableHelpers.deepClone(milestoneData.infos)
    local mustHaves = {}
    if gameConfig.milestones.randomizer.guranteeHoverlord and gameConfig.setup.venus then
      mustHaves = { "Hoverlord" }
    end

    for _, guid in pairs(gameState.milestoneGuids) do
      local milestoneInfo = awardAndMilestoneFunctions.getNextRandomInfo(transientState.latestMilestones, mustHaves)
      milestoneSystem.changeMilestone(guid, milestoneInfo)
    end

    transientState.milestoneRandomizerWorking = false

    return 1
  end

  startLuaCoroutine(self, "randomizeCoroutine")
end

milestoneSystem.spawnMilestoneTile = function(milestoneInfo)
  local milestonePlate = gameObjectHelpers.getObjectByName("milestonePlate")

  local milestoneDefaultTile = gameObjectHelpers.getObjectByName("milestoneDefaultTile")
  local scale = milestoneDefaultTile.getScale()

  local cloneCallback = function(clonedGuid)
    local clonedTile = getObjectFromGUID(clonedGuid)
    clonedTile.setLock(false)
    clonedTile.interactable = false

    Wait.time(function()
      clonedTile.setLock(true)
      clonedTile.setScale(scale)
      table.insert(gameState.milestoneGuids, clonedGuid)

      if milestoneInfo ~= nil then
        milestoneSystem.changeMilestone(clonedGuid, milestoneInfo)
      end
    end,2)
  end

  local nextSpawnPositionLocal = milestonePlate.call("getNextSpawnPosition")

  if nextSpawnPositionLocal == nil then
    logging.broadcastToAll("Unable to spawn milestone tile: All milestone spaces are occupied. This shouldn't happen, please report as a bug in Discord. Please create a save and attach it to the bug message/report.")
    return
  end

  local nextSpawnPositionWorld = vectorHelpers.fromLocalToWorld(milestonePlate, nextSpawnPositionLocal)
  createClonableObject(milestoneDefaultTile, nextSpawnPositionWorld, {0,180,0}, cloneCallback, true)
end

milestoneSystem.claimMilestone = function(tmPlayer, milestoneName, milestonePosition, tileGuid)
  function claimMilestoneCoroutine()
    local claimedMilestones = #gameState.claimedMilestones + 1
    local milestonePlate = gameObjectHelpers.getObjectByName("milestonePlate")

    local localClaimPosition = vectorHelpers.addVectors(milestonePlate.call("getClaimLocalPositions")[claimedMilestones], {0,0.15,0})
    local nextFreeClaimPosition = vectorHelpers.fromLocalToWorld(milestonePlate, localClaimPosition, true)

    local markers = getObjectFromGUID(getPlayerByColor(tmPlayer.color).playerArea.playerMat).call("getPlayerMarkerSource")
    local claimMarker = markers.takeObject({ position = nextFreeClaimPosition })
    coroutine.yield(0)
    coroutine.yield(0)
    local milestoneMarker = markers.takeObject({ position = vectorHelpers.addVectors(milestonePosition, {0,0.4,0.3}) })
    milestoneMarker.interactable = false
    milestoneMarker.setLock(true)

    coroutine.yield(0)
    coroutine.yield(0)
    logging.printToAll("Player "..tmPlayer.color.." claimed "..milestoneName, {1,1,1,1}, loggingModes.important)
    table.insert(gameState.claimedMilestones,
      { owner = tmPlayer.color,
        milestoneName = milestoneName,
        index = claimedMilestones,
        tileGuid = tileGuid,
        cubeGuids = { milestoneMarker.getGUID(), claimMarker.getGUID() } } )

    milestoneSystem.updateTokens()
    gameState.wasUpdated = true
    gameObjectHelpers.getObjectByName("milestonePlate").call("addClaim")

    return 1
  end

  startLuaCoroutine(self, "claimMilestoneCoroutine")
end

milestoneSystem.updateTokens = function()
  for i, guid in pairs(gameState.milestoneGuids) do
    local obj = getObjectFromGUID(guid)
    if #gameState.claimedMilestones == gameState.milestones.maxClaims then
      obj.call("removeAllButtons")
    else
      obj.call("updateButtons")
    end
  end
end

milestoneSystem.unclaimMilestone = function(milestoneIndex)
  if milestoneIndex ~= #gameState.claimedMilestones then
    logging.printToAll("Only allowed to revoke the last claimed milestone.", {1,1,1,1}, loggingModes.important)
    return false
  end

  for i, info in pairs(gameState.claimedMilestones) do
    if info.index == milestoneIndex then
      local  activationEffects = { resourceValues = { Credits = 8 } }

      objectActivationSystem_doAction({playerColor = info.owner,
        sourceName = "revoking the claim on a milestone",
        activationEffects = activationEffects})

      for _, guid in pairs(info.cubeGuids) do
        local obj = getObjectFromGUID(guid)
        if obj ~= nil then
          obj.destruct()
        end
      end

      getObjectFromGUID(info.tileGuid).call("unclaim")

      gameState.claimedMilestones[i] = nil
      logging.printToAll("Player "..info.owner.." revoked claim on '"..info.milestoneName.."'", {1,1,1,1}, loggingModes.important)
    end
  end

  milestoneSystem.updateTokens()

  gameState.wasUpdated = true

  return true
end

milestoneSystem.changeMilestoneSet = function(setIndex)
  function changeMilestoneSetCoroutine()
    if #gameState.milestoneGuids ~= 5 then
      milestoneSystem.setupMilestones(5)
    end

    local counter = 5
    while counter > 0 do
      counter = counter - 1
      if transientState.setupMilestonesOngoing then
        counter = 30
      end
      coroutine.yield(0)
    end

    if gameConfig.milestones.randomizer.enabled then
      transientState.latestMilestones = tableHelpers.deepClone(milestoneData.infos)
    else
      if gameState.setupIsDone and gameConfig.setup.venus then
        milestoneSystem.spawnMilestoneTile(milestoneData.infos.Hoverlord)
      end
    end

    for i=1,5 do
      local guid = gameState.milestoneGuids[i]
      local milestoneInfo = milestoneData.getMilestoneInfoByName(milestoneData.sets[setIndex][i])
      milestoneSystem.changeMilestone(guid, milestoneInfo)

      if gameConfig.milestones.randomizer.enabled then
        transientState.latestMilestones[milestoneData.sets[setIndex][i]].dealt = true
      end
    end

    return 1
  end

  startLuaCoroutine(self, "changeMilestoneSetCoroutine")
end

milestoneSystem.changeMilestone = function(guid, milestoneInfo)
  local obj = getObjectFromGUID(guid)
  obj.setDescription(milestoneInfo.description)
  obj.setName(milestoneInfo.name)
  local customization = {}
  customization.image = milestoneInfo.imageUrl
  obj.setCustomObject(customization)
  local reloadedObj = obj.reload()

  if gameConfig.milestones.randomizer.enabled then
    Wait.frames(|| reloadedObj.call("enableChangeButton"),2)
  end

  Wait.frames(|| reloadedObj.call("setClaimButtonTooltip", milestoneInfo.tooltip), 2)
end

milestoneSystem.turmoilSpecialHandling = function()
  for i, guid in pairs(gameState.milestoneGuids) do
    if milestoneData.infos.Terraformer.name == getObjectFromGUID(guid).getName() then
      milestoneSystem.changeMilestone(guid, milestoneData.infos.TurmoilTerraformer)
      return
    end
  end
end

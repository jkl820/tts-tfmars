#include ../staticData/eventData
#include ../buildingBlocks/baseOwnableObject
#include ../libs/vectorHelper
#include ../staticData/loggingData

defaultObjectState.active = false
defaultObjectState.tileObject = { adjacenyEffects = {}, activationEffects = {}, objectName = "" }

doubleClick = false

activationProperties = {}

function loadCallback(isSaveGame)
  updateButtons()
end

function getAdjacencyEffects()
  return objectState.adjacenyEffects
end

function createButtons()
  if type(initialize) == "function" then
    initialize()
  end
  if not objectState.active then
    createActivateButton()
  end
end

function activateObjectRemotely(params)
  activateObject(nil, params.playerColor, false)
end

function activateObjectIsDoubleClick()
  function catchDoubleClicksCoroutine()
    for i=1, 120 do
      coroutine.yield(0)
    end

    doubleClick = false

    return 1
  end

  if doubleClick == true then
    log("Catched a double click")
    return true
  end

  startLuaCoroutine(self, "catchDoubleClicksCoroutine")

  doubleClick = true
  return false
end

function activateObject(_, player_color, altClick)
  if not self.resting and player_color ~= nil then
    Global.call("logging_broadcastToColor", {
      message = "Activation failed: "..self.getName().." tile has to rest.",
      playerColor = player_color,
      messageColor = player_color,
      loggingMode = loggingModes.exception
    })
    return
  end

  if activateObjectIsDoubleClick() then
    return
  end

  if objectState.isNeutral then
    objectState.tileObject.owner = "NoOwner"
  else
    objectState.tileObject.owner = player_color
  end

  objectState.tileObject.guid = self.guid

  if objectState.ownableObjectName ~= nil and (objectState.tileObject.objectName == "" or objectState.tileObject.objectName == nil) then
    objectState.tileObject.objectName = objectState.ownableObjectName
  end

  if preObjectActivation ~= nil then
    preObjectActivation(playerColor)
  end

  local isAllowed = Global.call("hexMap_applyPlacementBonuses", {position = self.getPosition(),
    playerColor = player_color,
    tileObject = objectState.tileObject,
    ignorePlacementEffects = altClick or objectState.ignorePlacementEffects,
    ignoreEffects = objectState.ignoreEffects,
    object = self })

  if isAllowed and player_color ~= nil then
    isAllowed = Global.call("objectActivationSystem_doAction", { playerColor = player_color,
      sourceName = self.getName(),
      object = self,
      activationEffects = objectState.tileObject.activationEffects, })
  end

  if not isAllowed and player_color ~= nil then
    Global.call("logging_broadcastToColor", {
      message = "Placing tile "..self.getName().." at the given location is not allowed.",
      playerColor = player_color,
      messageColor = player_color,
      loggingMode = loggingModes.exception
    })
    return
  end

  removeButtons()
  Wait.condition(function()
    self.setLock(true)
    self.interactable = false
    updateButtons(true)
  end, function() return self.resting end)

  objectState.active = true

  onActivate(player_color, false)

  if onPostActivate ~= nil then
    onPostActivate(player_color)
  end

  if not objectState.isNeutral then
    registerPlayerOwnedObject(player_color)
  end

  objectState.wasUpdated = true

  self.script_state = onSave()
end

function deactivateObject(_,playerColor, altClick)
  if not altClick then
    Global.call("logging_broadcastToColor", {
      message = "Right click in order to remove object from tile.",
      playerColor = playerColor,
      messageColor = playerColor,
      loggingMode = loggingModes.important
    })
    return
  end

  local isAllowed = Global.call("hexMap_removeTileObject", {
    position = self.getPosition(),
    playerColor = playerColor,
    tileObject = objectState.tileObject,
    ignorePlacementEffects = objectState.ignorePlacementEffects }
  )

  if not isAllowed then
    return
  end

  if not objectState.isNeutral then
    unregisterPlayerOwnedObject(playerColor)
  end

  if onDeactivate ~= nil then
    onDeactivate(playerColor)
  end

  local marker = getObjectFromGUID(objectState.markerGuid)
  if marker ~= nil then
    marker.destruct()
  end

  objectState.active = false
  objectState.wasUpdated = true

  self.setLock(false)
  self.interactable = true

  updateButtons()
end

function removeButtons()
  local buttons = self.getButtons()
  if buttons ~= nil then
    for i=#buttons, 1, -1 do
      self.removeButton(buttons[i].index)
    end
  end
end

function createActivateButton()
  self.createButton(buttonSetup.activateButton)
end

function updateButtons(skipCleanup)
  -- if e.g. the calling function already removes the buttons preemptively
  if not skipCleanup then
    removeButtons()
  end

  if objectState.active and self.getLock() then
    for i, buttonInfo in pairs(buttonSetup.deactivateButtons) do
      self.createButton(buttonInfo)
    end
  elseif not objectState.active then
    self.createButton(buttonSetup.activateButton)
  end
end

function onActivate(player_color, altClick)
  local marker = Global.call("placePlayerMarker", { playerColor = player_color,
    position = vectorHelpers.addVectors(self.getPosition(), {0,0.2,0}),
    objectName = objectState.ownableObjectName,
    objectGuid = self.guid})

  Wait.frames(function()
    objectState.markerGuid = marker.getGUID()
    objectState.wasUpdated = true
  end, 5)
end

buttonSetup = {}

buttonSetup.activateButton = {
        label="Mark",
        click_function="activateObject",
        tooltip = "Click to confirm placement (locks the object in place).",
        function_owner=self,
        position={0, 0.15, 0}, --0.25
        rotation={0,270,0},
        height=500,
        width=500,
        alignment = 0,
        scale={x=1, y=1, z=1},
        font_size=150,
        font_color={0/255,0/255,0/255,1},
        color={145/255,180/255,5/255,0.85}
}

buttonSetup.deactivateButtons = {
  {
    label="",
    click_function="deactivateObject",
    tooltip = "Right click to undo placement.",
    function_owner=self,
    position={0, 0.15, 0}, --0.25
    rotation={0,270,0},
    height=100,
    width=100,
    alignment = 0,
    scale={x=1, y=1, z=1},
    font_size=150,
    font_color={0/255,0/255,0/255,1},
    color={255/255,40/255,40/255,1}
  }
}

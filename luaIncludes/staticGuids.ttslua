bags = {
	venusBag = "0818d8",
	coloniesBag = "ebe13a",
	coloniesShipBag = "b176ad",
	coloniesMarkersBag = "b308cf",
	pathFinderBag = "323d14",
	oceanBag = "d6af9e",
	specialsBag = "0d97f2",
  genericDelegateBag = "deb215",
}

bagProtectorGuidStores.normalBags = bags

referenceBags = {
	containerOne = "3340b2",
	containerTwo = "9ee3f2"
}

bagProtectorGuidStores.referenceBags = referenceBags

cardAutomaton = {
  baseCard = "e77dbd",
  baseBlueCard = "e77dbe",
  baseGreenCard = "e77dbf",
  baseEventCard = "e77dc0",
  baseInfrastructureCard = "e77dc1",
  baseCorporationCard = "e77dc2",
}

turmoilPartyPlates = {
  MarsFirst = "ad4d18",
  Scientists = "ce0555",
  Unity = "35bec1",
  Greens = "99259c",
  Reds = "995445",
  Kelvinists = "dafd56",
}

expansions = {
	-- Venus
	venusProjects = "b530b4",
	venusCorps = "d76fa5",
  venuphileAwardTile = "d4e451",
  hoverlordMilestoneTile = "404512",
  venusTerraformingTrack = "59dd23",
  venusTerraformingMarker = "ca3d97",

	-- Colonies
  coloniesTradingTile = "3e7af8",
	coloniesProjects = "211e80",
	coloniesCorps = "cdef51",
  coloniesTradeFleetTile = "9ba254",

	-- Prelude
	preludeProjects = "400808",
	preludeCorps = "4b4231",

	-- turmoil
  turmoilPartyTile = "2baba5",
	turmoilProjects = "160ff4",
	turmoilCorps = "2be67c",
	turmoilDeck = "87a219",
	turmoilGlobalEventDeck = "87a219",
	turmoilColonyCards = "f8b2f4",
	turmoilVenusCards = "899f9f",
	turmoilColonyVenusCards = "192cfa",
	turmoilTMToken = "a9aaf0",

  turmoilTile = "fa0471",
	turmoilBoard = "12612d",
	turmoilEventBoard = "fc9a89",
	turmoilNeutralDelegates = "7caf29",
	turmoilBasePartyPlate = "99259c",
	turmoilDominanceMarker = "578734",
  turmoilGenericDelegate = "2b66ef",

  -- Venus Phase Two
  venusPhaseTwoDeck = "23d12e",

  -- Solaris
  solarisDeck = "d1001d",

  -- Ares
  aresDeck = "aed180",

	-- Promo Cards
	promoCorps = "fcd67e",
	promoPreludes = "4dea81",
	promoProjects = "c632e8",

	-- Corp Era
	corpEraProjects = "f34f65",
	corpEraCorps = "1a43a4",

	-- PathFinders
	pathFinderProjects = "f0cf60",
	pathFinderCorps = "bf570f",
	pathFinderPreludes = "56f4b8",

  -- High Orbit
  highOrbitInfrastructureDeck = "8bf4f7",

	-- Xenos
	xenosCorps = "97b1d0",

	-- Fan made
	fanMadeProjects = "95f620",
}

setupGuids = {
  -- Setup
  playmat = "9a571f",
  draftBag = "b0beba",
  setupMat = "2bd7cb",
  standardProjects = "270e01",

  -- Zones
  boardZone = "afc408",
  tradeFleetZone = "41343b",

  -- Counters
  generationCounter = "1c4a11",
  cityCounter = "8df3e7",
  citiesOnMarsCounter = "c6c23e",
  oceanCounter ="e260a7",

  capitalCityToken = "0e17f3",
  oceanCityToken = "4cfb25",

  genericScoreBoard = "091112",
  genericTRCube = "7ddd14",
  genericClassicPlayerBoard = "583d53",
  genericPlayerBoard = "13d3bb",
  genericPlayerBoardFlipped = "13d3be",
  genericDraftingToKeepTile = "7a62d8",
  genericDraftingCheckToken = "25ad34",
  genericIconTableau = "0adaf2",
  genericPathfinderIconTableau = "e4ebb6",
  genericHighOrbitIconTableau = "e4ebb7",
  genericActivationTableau = "52f95e",
  generationMarker = "a63f90",
  genericPlayerAntiLagBoard = "bd6888",
  genericPlayerOrgHelpBoard = "081edb",
  milestonePlate = "8f1595",
  milestoneDefaultTile = "ca6fe4",
  milestoneAndAwardDefaultTile = "ca6fe4",
  awardPlate = "8f1596",
  baseBonusTokenGuid = "a4d4c3",
  mainBoardTile = "71ca01",

  -- Stuff to use
  firstPlayerToken = "2f276a",

  -- Infinite Bags
  genericOcean = "e3a5e4",
  genericCityBag = "f38446",
  genericGreeneryBag = "e4c505",
  genericBronzeTokenBag = "189729",
  genericSilverTokenBag = "da7557",
  genericGoldTokenBag = "4445e1",
  animalWildSource = "716168",
  microbeFloaterSource = "4499d3",
  scienceFighterSource = "411a07",
  oreSource = "716169",
  dataAsteroidSource = "9dec75",
  actionMarkersSource = "3cc625",
  repeatActionRepeatPreludeTokenSource = "3cc628",
  repeatEventTokenSource = "09d2a41",
  resourceWildTokenSource = "3cc626",
  programableActionTokenSource = "3cc627",
  floatingArrayBag = "bb5da3",
  gasMineBag = "080f86",
  venusHabitatBag = "7f9cef",
  duststormBag = "d749f0",
  erosionBag = "fe18b7",

  -- Counters
  extraOceansCounter = "e260a7",

  -- Tokens
  oxygenToken = "ca3d95",
  temperatureToken = "f1bfac",
  oceanToken = "ca3d96",
  scrappingToken = "ca3d97",
  venusPathfinderToken = "0683d4",
  earthPathfinderToken = "92ae13",
  marsPathfinderToken = "d45204",
  jovianPathfinderToken = "6a30b4",

  -- GameEndZones
  oxygenMax = "2cc3ab",
  tempMax = "ef3cb2",

  scoreCounterBag = "c1fd7a",

  -- For shuffling
  projectStackTile = 'b550fe',
  projectDiscardTile = "f7e628",
  projectZone = "1ae58d",
  revealZone = "1ae58e",
  discardStackTile = '0e0cb2',

  corpAmountToken = "c1af2b",

  -- Base
  loggingTile = "9a5720",
  pathfinderBoard = "9ebcf8",
  projectDeck = "5a3113",
  corpDeck = "7f05e7",
  preludeDeck = "501238",
  gameObjectsBoard = "466283",
  gameMap = "e0de83",
  marsMapTile = "e0de83",
  venusMapTile = "76225d",
  gameBoardTile = "71ca01",
  standardProjectTile = "71ca02",
  gameConfigTile = "cb2a6c",
  timerConfigTile = "9460b1",
  -- Functionality appears to be incomplete for now
  -- banMePile = "e3a5e5",
}

specialTiles = {
  preserve = "8617b9",
  miningArea = "617acd",
  miningRights = "3de716",
  nuclear = "8df758",
  restricted = "59c9a4",
  capital = "0e17f3",
  lavaFlows = "a40228",
  ecologicalZone = "c58448",
  industrial = "676cf5",
  mohole = "b69b74",
  commercial = "11528c",
  deimosDown = "c58449",
  greatDam = "8df759",
  magneticFieldGenerators = "b69b75",
  crashSite = "e56912",
  newVenice = "4cfb25",
  oceanGreenery = "919a9d",
  wetlands = "919a9d",
  redCity = "4bab93",
  MenagerieTile = "80c18b",
  AresCapital = "be9df4",
  AresCommercialDistrict = "c66e48",
  AresEcologicalZone = "b46a9e",
  AresFertilizerFactory = "1c6b35",
  AresIndustrialCenter = "793527",
  AresMeteorCrater = "9b23ce",
  AresMiningAreaSteel = "2627ee",
  AresMiningAreaTitanium = "568ea3",
  AresMiningRightsSteel = "697619",
  AresMiningRightsTitanium = "b212e7",
  AresMoholeArea = "eef791",
  AresNaturalPreserve = "7ee900",
  AresNuclearZone = "3ea477",
  AresOceanFarm = "943c58",
  AresOceanicCity = "ea6814",
  AresOceanSanctuary = "223447",
  AresRestrictedArea = "2b26f1",
  AresSolarFarm = "6ae25b",
  AresVolcano = "87136d",
}

gameObjectGuidStores.expansions = expansions
gameObjectGuidStores.setupGuids = setupGuids
gameObjectGuidStores.bags = bags
gameObjectGuidStores.cardAutomaton = cardAutomaton
gameObjectGuidStores.specialTiles = specialTiles

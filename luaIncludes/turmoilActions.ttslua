function turmoilReduceTR()
  if isDoubleClick("turmoilReduceTR") then
    return
  end

  function turmoilReduceTRCoroutine()
    transientState.turmoilActions.trRevisionInProgress = true

    for i,player in pairs(gameState.allPlayers) do
      if not player.neutral then
        decreasePlayerTerraforming(i, "TR Revision")
      end
    end

    transientState.turmoilActions.trRevisionInProgress = false
    return 1
  end

  startLuaCoroutine(self, "turmoilReduceTRCoroutine")
end

function turmoilGlobalEvent(_, playerColor, altClick)
  if isDoubleClick("turmoilGlobalEvent") then
    return
  end

  function turmoilGlobalEventCoroutine()
    while transientState.turmoilActions.trRevisionInProgress do
      coroutine.yield(0)
    end
    transientState.turmoilActions.globalEventInProgress = true


    calculateInfluence()
    local globalEventZone = getObjectFromGUID(gameState.turmoilData.globalEvents.currentZone)

    for _,object in pairs(globalEventZone.getObjects()) do
      if object.tag == "Card" and not object.is_face_down then
        if object.getVar("activateGlobalEvent") ~= nil then
          object.call("activateGlobalEvent", { allPlayers = gameState.allPlayers, triggeredByColor = playerColor })
          if object.getVar("resolveGlobalEvent") ~= nil then
            object.call("resolveGlobalEvent", { allPlayers = gameState.allPlayers, triggeredByColor = playerColor })
          end

          for i=1, 120 do
            coroutine.yield(0)
          end
          object.flip()
          transientState.turmoilActions.globalEventInProgress = false
          return 1
        else
          logging.broadcastToAll("Global Event not implemented yet. Please do it manually!", {1,0,0}, loggingModes.exception)
          transientState.turmoilActions.globalEventInProgress = false
          return 1
        end
      end
    end

    local turmoilCardDeckExists = false
    for _, object in pairs(globalEventZone.getObjects()) do
      if object.tag == "Deck" then
        turmoilCardDeckExists = true
      end
    end

    if turmoilCardDeckExists then
      logging.broadcastToAll("Global event cards are stacked face up. This breaks the global event handling during solar phase.", {1,0,0}, loggingModes.exception)
    end

    transientState.turmoilActions.globalEventInProgress = false
    return 1
  end

  startLuaCoroutine(self, "turmoilGlobalEventCoroutine")
end

function turmoilNewGovernment(_, playerColor, _)
  if isDoubleClick("turmoilNewGovernment") then
    return
  end

  function turmoilNewGovernmentCoroutine()
    while transientState.turmoilActions.globalEventInProgress or
      transientState.turmoilActions.trRevisionInProgress do
      coroutine.yield(0)
    end
    transientState.turmoilActions.newGovInProgress = true

    local cleanupChairman = function()
      local currentChairman = getChairman()
      if currentChairman ~= nil then
        cleanUpDelegate(currentChairman)
      end
    end

    local placeNewChairman = function(partyInfo)
      local delegate = getFirstDelegateInZone(partyInfo.partyLead.zoneGuid)

      if delegate == nil then
        return
      end

      local delegateColor = delegate.getDescription()
      delegate.setPositionSmooth(gameState.turmoilData.chairman.transform.pos)

      if delegateColor ~= "Neutral" then
        local leadingPlayer = getPlayerByColor(delegateColor)
        logging.broadcastToAll("New chairman of the commitee is "..leadingPlayer.name, leadingPlayer.color, loggingModes.essential)
        increasePlayerTerraforming(getPlayerIndexByColor(delegateColor), "player puts up the new chairman.")
      else
        logging.broadcastToAll("New chairman of the commitee is unaffiliated", "Grey", loggingModes.essential)
      end
    end

    local changeGoverningParty = function(partyInfo)
      for _, data in pairs(turmoilPartyData.parties) do
        if data.id == partyInfo.partyId then
          data.onFactionTakesOver(gameState.allPlayers)
          gameState.turmoilData.rulingPartyId = data.id
          break
        end
      end

      placeNewChairman(partyInfo)
      changePartyPlateToParty(partyInfo)
    end

    local cleanupDominatingPartyDelegates = function(partyInfo)
      for _, object in pairs(getObjectFromGUID(partyInfo.mainPartyZone).getObjects()) do
        if object.getName() == "Delegate" then
          cleanUpDelegate(object)
        end
      end
    end

    recalculateDominance()
    recalculatePartyLeads()

    cleanupChairman()
    local dominatingParty = getDominatingParty()

    for i=1,45 do coroutine.yield(0) end
    changeGoverningParty(dominatingParty)
    for i=1,45 do coroutine.yield(0) end
    cleanupDominatingPartyDelegates(dominatingParty)
    for i=1,30 do coroutine.yield(0) end
    refreshLobby()
    for i=1,40 do coroutine.yield(0) end
    recalculateDominance()
    for i=1,40 do coroutine.yield(0) end
    transientState.turmoilActions.newGovInProgress = false

    return 1
  end

  startLuaCoroutine(self, "turmoilNewGovernmentCoroutine")
end

function turmoilChangingTimes()
  if isDoubleClick("turmoilChangingTimes") then
    return
  end

  function moveComingEventAndPlaceCurrentNeutralDelegate(comingZone, neutralDelegateBag)
    for _,object in pairs(comingZone.getObjects()) do
      if object.tag == "Card" then
        placeDelegateFromBagInParty(neutralDelegateBag, extractPartiesFromDescription(object.getDescription()).currentParty)
        local newPosition = vectorHelpers.addVectors(gameState.turmoilData.globalEvents.positions.current, {0,0.5,0})
        object.setPositionSmooth(newPosition)
      end
    end
  end

  function moveDistantEvent()
    local turmoilDeck = gameObjectHelpers.getObjectByName("turmoilDeck")
    if turmoilDeck == nil then
      return
    end

    local newPosition = vectorHelpers.addVectors(gameState.turmoilData.globalEvents.positions.coming, {0,0.5,0})
    turmoilDeck.takeObject({
      position = newPosition
    })
  end

  function placeDistantDelegate(neutralDelegateBag)
    local turmoilDeck = gameObjectHelpers.getObjectByName("turmoilDeck")
    if turmoilDeck == nil then
      return
    end

    local remainingCards = turmoilDeck.getObjects()
    local object = remainingCards[#remainingCards]
    placeDelegateFromBagInParty(neutralDelegateBag, extractPartiesFromDescription(object.description).distantParty)
  end

  function changingTimesCoroutine()
    log(transientState.turmoilActions)
    while transientState.turmoilActions.newGovInProgress or
      transientState.turmoilActions.trRevisionInProgress or
      transientState.turmoilActions.globalEventInProgress do
      coroutine.yield(0)
    end
    transientState.turmoilActions.changingTimesInProgress = true

    recalculateDominance()

    local neutralDelegateBag = getObjectFromGUID(gameState.turmoilData.neutralBagId)
    local comingZone = getObjectFromGUID(gameState.turmoilData.globalEvents.comingZone)

    moveComingEventAndPlaceCurrentNeutralDelegate(comingZone, neutralDelegateBag)
    for i=1,30 do coroutine.yield(0) end
    recalculateDominance()
    for i=1,50 do coroutine.yield(0) end
    moveDistantEvent()
    for i=1,40 do coroutine.yield(0) end
    placeDistantDelegate(neutralDelegateBag)
    for i=1,20 do coroutine.yield(0) end
    recalculateDominance()
    for i=1,20 do coroutine.yield(0) end
    recalculatePartyLeads()
    for i=1,20 do coroutine.yield(0) end

    transientState.turmoilActions.changingTimesInProgress = false
    return 1
  end

  startLuaCoroutine(self, "changingTimesCoroutine")
end

function activatePolicy(playerColor)
  local partyInfo = getRulingParty()

  local policyBonus = turmoilPartyData.parties[partyInfo.partyId].policyBonus

  if policyBonus.oneTimeEffect then
    for _, color in pairs(gameState.turmoilData.oneTimeEffectTable) do
      if color == playerColor then
        return
      end
    end
  end

  local turmoilTile = gameObjectHelpers.getObjectByName("turmoilTile")

  local isSuccess = objectActivationSystem_doAction({
    playerColor = playerColor,
    sourceName = policyBonus.friendlyName,
    object = turmoilTile,
    activationEffects = policyBonus.actionProperties
  })

  if policyBonus.oneTimeEffect and isSuccess then
    table.insert(gameState.turmoilData.oneTimeEffectTable, playerColor)
  end

  if not isSuccess and turmoilPartyData.parties[partyInfo.partyId].onPolicyActionNotAllowed ~= nil then
    turmoilPartyData.parties[partyInfo.partyId].onPolicyActionNotAllowed(getPlayerByColor(playerColor))
  end
end

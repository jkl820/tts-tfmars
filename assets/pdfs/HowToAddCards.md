# How To Script New Cards Via Card Description

This guide will tell you show the basics on how to script new cards in the TFMars mod via a card's description text.

Not everything is possible by just using a card's description, but you can implement alot that way nonetheless, including actions and automatically triggered effects.

## Card Name

Every card has to have card name. Depending on the type of card it also has an identifier behind the name in parenthesis.

| Card Type | Identifier In Card Name |
|-|-|
| Green Card | (G) |
| Blue Card | (B) |
| Event Card | (E) |
| Corp Card | (C) |
| High Orbit Card | (HO) |
| Prelude | n.a. |

Examples

Fusion Power (G)

Spin-Off Department (B)

Background: When a card is drawn from a deck by any means and the card has no script a script from the corresponding base card (each type has one) is used instead. There are six different base cards: BaseCard (if n.a.), GreenBaseCard, BaseBlueCard, BaseEventCard, BaseHighOrbitInfrastructureCard, BaseCorpCard

## Card Description

This gets a bit more complicated than the last chapter. An overview what you can add to a card description in order to realize a card immediate effects, actions and triggered effects.

From here on the single character 'N' represents a natural number. The following subchapters all begin with examples. The word before the colon has no wiggle room. If you write something differently (e.g. 'Requirements:' instead of 'Reqs:') the parser will just ignore your input in that row.

### Card Cost

Examples

Cost: 4 ==> Card costs 4 Credits

Cost: 4 Titanium ==> Card costs 4 Titanium

That's it. You can specify the resource type in the cost row, it defaults to Credits if you don't.

### Card Requirements

Examples

Reqs: -20 MaxTemp ==> Temperature has to be 20 degrees or lower

Reqs: -20 Temp ==> Temperature has to be at least 20 degrees

Reqs: 2 AnyCities ==> Requires at least two cities in game

Reqs: 2 AnyMaxCities ==> Requires that a maximum of two cities exist in game.

Reqs: 2 OwnCities ==> You have to own two cities

Reqs: 1 OtherTitanium ==> Requires that another player has 1 Titanium production(!)

Here are the lists of all possible values. You can combine 'RequirementTargets' and the 'Max' keyword. The values of a requirement will be checked against the requirement target's properties. Global requirements (like temperature) don't support a requirement target.

| Requirement Target | Description Keyword |
|-|-|
| Player themself | Own |
| All players | Any |
| Other players | Other |
| Only one other player | OtherSingle |
| No Player | Neutral |

| Requirement Types | Description Keyword |
| - | - |
| Ocean in game | Ocean |
| Temperature | Temp |
| Oxygen | O2 |
| Venus TF track | TFVenus |
| Owned Tags | >> See Card Tags List << |
| Owned Objects | >> See Owned Objects List << |

### Card Tags

Examples

Tags: Space Event ==> just gives an event tag

Tags: Building Space Power Science Jovian Earth Venus Plant Microbe Animal City WildCard ==> gives every tag except 'None' and 'Event'

If you omit the Tags row the card has no tags at all and counts as a 'None' tagged card (for 'Community Service').

If you define a new tag that tag will be counted internally - unless somebody implements an icon tableau for that new tag.

### Resource Values

Examples

Resrc: 4 Plants ==> adds 4 plants to your storage

Resrc: -2 Plants ==> removes 2 plants from your storage and if you don't have enough you are not allowed to play the card

Allowed values: Credits, Steel, Titanium, Plants, Energy, Heat

### Production values

Examples

Prod: 1 Credits ==> increases your ME production by one

Prod: -1 Energy ==> decreases your Energy production by one and forbids card activation if you don't have enough energy production

Allowed values: Credits, Steel, Titanium, Plants, Energy, Heat

### Effects

Examples

Effects: City Temp TFVenus Ocean O2 ==> gives you an ocean, a city tile, increases the TF Venus track by one as well as temperature and oxygen

Effects: Greenery ==> gives you one greenery

Effects: DrawCard DrawCard DrawCard ==> draw three cards

The card interpreter will not handle numbers in this row and just throw exceptions. If a card has the same effect multiple times you have to write them out one by one.

Allowed values: DrawCard, DiscardCard, City, Temp, O2, TFVenus, TR, Animal, WildCardToken, Floater, Microbe, Science, Asteroid, Data, Fighter, Habitat, Ore, ShuffleDiscard, Ocean, TradeToken

There are other allowed values, but most of them are one time effects like all special tiles.

### Victory Points

Examples

VP: 4 ==> just gives 4 victory points at the end of the game

VP: 1Per2Counter ==> gives 1 victory point per card resource divided by 2

VP: 1Per1Counter ==> gives 1 victory point per card resource

VP: 1Per1Jovian ==> gives 1 victory point for each owned Jovian tag

VP: 1Per2Colony ==> gives 1 victory point for each second colony in play

Examples cover most stuff.

### Counters

Examples

Counter1: BaseCounter ==> first counter is a base counter

Counter2: VPCounter ==> second counter is a vp counter

CounterType: Floater ==> Card may collect floaters and colors are adjusted accordingly

---

Counter1: BaseCounter Size (200,200) Pos (1,0,0) ==> first counter is a base counter of the specified size (width,height) and at the given relative position on the card (1,0,0).

Counter2: VPCounter Pos (0,0,-0.2) ==> second counter is a vp counter at the specified location.

CounterType: Asteroid ==> Card may collect asteroid card resources and colors are adjusted accordingly

### Actions

Examples

Action1: -> 1 Counter ==> gain one card resource on the card, requires a counter of course

Action2: 1 Energy -> 1 Counter ==> spend one energy to gain one card resource

Action3: 1 Energy -> DrawCard ==> spend one energy to draw one card

Action4: 1 EnergyProd -> TFVenus ==> reduce energy production by one to increase the TF Venus track by one

Action5: 2 Counter -> Temp ==> spent two card resources to increase temperature by one step

AllowedValues: All 'Effects' from above and also combinations of value + resource types are allowed. The suffix 'Prod' tells the interpreter that production is affected, not storage.

### Event Handlers

Examples:

Action1: -> DrawCard ==> draw a card without a cost
Action1Props: isRepeatable ==> Action1 can be used multiple times in a single generation
EventHandler1: playerThemself payTwentyCostCard -> Action1 ==> owner executes action '1' if he/she plays a card that has a base cost of 20 or more

All possible event types are:

cityPlayed
spaceCityPlayed
marsCityPlayed
greeneryPlayed
oceanPlayed
colonyPlayed
productionChanged
venusTerraformed
oxygenIncreased
buildingTagPlayed
spaceTagPlayed
powerTagPlayed
scienceTagPlayed
jovianTagPlayed
earthTagPlayed
venusTagPlayed
plantTagPlayed
microbeTagPlayed
animalTagPlayed
noneTagPlayed
eventTagPlayed
marsTagPlayed
infrastructureTagPlayed
vpCardPlayed
animalResourceGained
microbeResourceGained
floaterResourceGained
scienceResourceGained
fighterResourceGained
dataResourceGained
asteroidResourceGained
payTwentyCostCard
standardProjectCity
standardProjectGreenery
standardProjectOcean
standardProjectTemperature
standardProjectPowerPlant
standardProjectVenus
standardProjectColony
cardWithRequirmentPlayed
specialTilePlayed
increasePathfinderVenus
increasePathfinderEarth
increasePathfinderMars
increasePathfinderJovian
terraformingGained
marsTilePlaced
turmoilFactionChanged
specialTilePlayed
productionPhase
newGeneration
cardPlayed
turmoilNewGovernment
actionPerformed
payedForCard
conversionRatesUpdated
playerTurnBegan
planetWildCardTokenAbsorbed
colonyTraded

Possible targets are:

playerThemself
anyPlayer
otherPlayers ==> rarely used
noPlayer ==> rarely used

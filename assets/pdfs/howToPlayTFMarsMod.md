# How To Play (And Randomizer Tiles)

Hi there, thanks for playing this mod. The intend of this HowTo is to lend you a hand with concepts and behaviours of this mod as not everything is completely intuitive.

We assume that you've done the TTS tutorial or at least have some experience with TTS. If not, we recommend the TTS tutorial to learn at least about basic movement in TTS.

This 'HowTo' assumes that you have choosen 'White' as player color (directions won't necessarily match otherwise)

---

## Player Area Overview

![alt text](tutorial_overview.png "Title")

- Drafting tile only spawns if you've enabled drafting
- Activation tableau and icon tableau only spawn if you've enabled 'Extended Scripting'

---

## Setup a game

On the central map you have three boards where you can select the expansions you want to add to your game (and some other settings which you can ignore for now). By default only the base game + corporate era are selected. The option 'Extended Scripting' should stay enabled (makes your life easier).

If you are happy with your selections just click on the 'Finish setup' button in order to finish the game setup phase.

---

## Starting the game

After you've clicked 'Finish Setup' you will see a button with the label 'Research' in the centre of the map. Click on it to actually start the game.

You will be dealt 10 projects cards, 2 corporations by default and 4 prelude cards if the 'Prelude' expansion is enabled.

You have a total of 8 hand zones (2 columns, 4 rows). Corporations are dealt to your main hand, which is also the hand where cards are discarded from. The 10 project cards are dealt to one of the 7 other hand zones.

Select the corp you want to play and place it on the 'Corporate HQ' space above your player mat and move all project cards that you don't want to keep into your main hand. If the card is facing down just press the 'F' on your keyboard to flip the card while either holding the card or hovering with the mouse cursor over the card. If you are ready click on the green button on your corporation. This will activate the corporation and automatically apply all immediate effects. Now click either on the 'A' button of your drafting tile (to the left of your player mat) or press the discard button in front of your player mat. The 'A' button will automatically deduct 3 credits per project card kept (project cards that are not in your main hand), the discard button just discards all cards from your main hand but nothing more.

To start the first generation action phase just click on the respective button on the top of your screen.

---

## Playing and paying cards during the action phase

Take a card from your hand, move it to the activation tableau (to the right of your player mat) and drop the card on the 'Activate' space.

At the top of the activation tableau you can decide how you want to pay for the given card.

If you have everything prepared just click on the green button in the middle of the card. You cannot undo that (except by rewinding time in TTS).

The resources you've choosen will be automatically deducted and all immediate effects will be applied as well.

Card effects (e.g. Rover Construction) will trigger automatically.

Remark 1: Some corporations are not yet fully implemented, you will have to trigger the effects manually. There aren't many, and they will get less over time but just so you know.

Remark 2: You can toggle the 'Activation Rules' on the bottom of the activation tableau. This can be useful should you encounter any problems with requirements checking.

If you want to try it out: Toggle the card activation rules once and play a card where you don't meet the requirements and toggle it back and try another card with requirements.

---

## Action cards

Action cards are quite self-explanatory. All actions have a green button that is transparent and overlays the action (you have to play and pay the respective action card first ofc). Press the button of the action you want to perform and everything is done for you.

---

## Ending turns and ending generations

At the top of your screen there's a button labeled 'End Turn'. Press this button if you've done your one to two actions. The next player gets a signal to do their own actions and this continues as long as at least one player didn't pass for the current generation.

If you don't have anything left to do in the current generation navigate to the small rocket above your player mat - hover over it and it should say '(1/2) Active!'. Press the key '2' on your keyboard while hovering over the generation rocket to change the state from active to inactive. Your rocket then should say '(2/2) Pass the Generation'. Now press 'End Turn' if you are the active player and if every other player has passed for the current generation a new generation will begin.

You can set your generation rocket to 'Active' by pressing '1' on your keyboard while hovering over the rocket (in case you forgot some action and the current generation didn't end yet).

---

## Drafting and next generation

When a new generation starts you will get 4 cards. If you are playing solo then just take all the cards you want from your main hand into one of your other 7 hands and press the 'A' labeled button on your 'Drafting Tile'.

If you are playing in multiplayer and drafting is enabled choose the card you want and put it face down on the 'TO KEEP' space on the drafting tile and click on the red 'X' token -> token is flipped to the green checkmark side. Repeat this process until you get the last card.

When you've received the last card decide which cards you want to keep and which you want to discard. If you are ready press the 'A' button on the drafting tile to pay for the cards you want to keep and to discard all cards from your main hand.

At the moment drafting/buying cards is part of the generation phase. This means you just start doing actions and ending turns after everyone has decided which cards they want to keep and discard.

That's all. Should you still have questions just ask in the MMGA discord channel. If you are new to the game you can stop reading here.

---
---

<div style="page-break-after: always;"></div>

## Randomizer Tiles

The following sections contain information and clarifications about special randomizer tiles.

<img src="extraTile_01.png" alt="drawing" width="200"/>

Idea taken from the Pathfinder fan expansion: Tiles with a blue triangle count as reserved for the purposes of placing oceans only. Other tiles can be placed normally here. The only tiles you cannot place here are tiles that are not oceans and have to be placed on an ocean reserved space (e.g. Mangrove).

---

<img src="extraTile_02.png" alt="drawing" width="200"/>

Tiles like this have a cost. You can decide to not pay the cost but then you won't gain the placement bonus from that tile either. This specific tile increases temperature by one step and lets you draw a card when you pay the placement cost of 3 energy resources. If you don't want to pay the cost and gain the placement reward just right click on the mark button. You will still get the adjacency bonus effects.

---

<div style="page-break-after: always;"></div>

## Clarifications

<img src="extraTile_04.png" alt="drawing" width="200"/>

When placing a tile pay 2 credits and gain two wildcard tokens that can be used as any card resource token except science. Both tokens have to be placed on the same card.

---

<img src="extraTile_09.png" alt="drawing" width="200"/>

This tile gives an adjacency bonus of one plant to each neighbouring tile. For this tile a special placement rule applies: You have to pay 5 credits in order to place a tile here, no exceptions.

---